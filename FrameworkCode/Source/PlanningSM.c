/****************************************************************************
 Module
   Planning.c

 Revision
   2.0.1

 Description
   This is a template file for implementing state machines.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/27/17 09:48 jec      another correction to re-assign both CurrentEvent
                         and ReturnEvent to the result of the During function
                         this eliminates the need for the prior fix and allows
                         the during function to-remap an event that will be
                         processed at a higher level.
 02/20/17 10:14 jec      correction to Run function to correctly assign 
                         ReturnEvent in the situation where a lower level
                         machine consumed an event.
 02/03/16 12:38 jec      updated comments to reflect changes made in '14 & '15
                         converted unsigned char to bool where appropriate
                         spelling changes on true (was True) to match standard
                         removed local var used for debugger visibility in 'C32
                         commented out references to Start & RunLowerLevelSM so
                         that this can compile. 
 02/07/13 21:00 jec      corrections to return variable (should have been
                         ReturnEvent, not CurrentEvent) and several EV_xxx
                         event names that were left over from the old version
 02/08/12 09:56 jec      revisions for the Events and Services Framework Gen2
 02/13/10 14:29 jec      revised Start and run to add new kind of entry function
                         to make implementing history entry cleaner
 02/13/10 12:29 jec      added NewEvent local variable to During function and
                         comments about using either it or Event as the return
 02/11/10 15:54 jec      more revised comments, removing last comment in during
                         function that belongs in the run function
 02/09/10 17:21 jec      updated comments about internal transitions on During funtion
 02/18/09 10:14 jec      removed redundant call to RunLowerlevelSM in EV_Entry
                         processing in During function
 02/20/07 21:37 jec      converted to use enumerated type for events & states
 02/13/05 19:38 jec      added support for self-transitions, reworked
                         to eliminate repeated transition code
 02/11/05 16:54 jec      converted to implment hierarchy explicitly
 02/25/03 10:32 jec      converted to take a passed event parameter
 02/18/99 10:19 jec      built template from MasterMachine.c
 02/14/99 10:34 jec      Began Coding
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "PlanningSM.h"
#include "DriveSM.h"
#include "MasterSM.h"
#include "ColorSensor.h"
#include "AccelSPIFSM.h"
#include "MasterSM.h"
#include "NormalOpsSM.h"
#include "SpudSPI.h"
/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE STATE_ZERO

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/
static ES_Event_t DuringRest( ES_Event_t Event);
static ES_Event_t DuringExploring( ES_Event_t Event);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static PlanningState_t CurrentState;
static bool makeDecision = false;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunPlanningSM

 Parameters
   ES_Event_t: the event to process

 Returns
   ES_Event_t: an event to return

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 2/11/05, 10:45AM
****************************************************************************/
ES_Event_t RunPlanningSM( ES_Event_t CurrentEvent )
{
   bool MakeTransition = false;/* are we making a state transition? */
   PlanningState_t NextState = CurrentState;
   ES_Event_t EntryEventKind = { ES_ENTRY, 0 };// default to normal entry to new state
   ES_Event_t ReturnEvent = CurrentEvent; // assume we are not consuming event
   ES_Event_t Event2Post;
   
   

   switch ( CurrentState )
   {
       case Rest :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringRest(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case INVALID_ROBOT_POS : //If event is event one
                  // Execute action function for state one : event one
                  NextState = Exploring;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
									printf("Invalid robot pos \r\n");
                  Event2Post.EventType = ES_SPIRAL_FORWARD;
                  Event2Post.EventParam = 200;
                  // Posting spiralling event to DriveSM
                  PostDriveSM(Event2Post);
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
               
               case MAKE_DECISION : //If event is event one
                  // Execute action function for state one : event one
                  printf("Decision made\r\n");
                  MakeDecision();
                  break;
                // repeat cases as required for relevant events
            }
         }
       break;
         
       case Exploring :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringExploring(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {     
            switch (CurrentEvent.EventType)
            {
               case ES_MOTION_COMPLETE: //If event is event one
                 if (CurrentEvent.EventParam == MC_SPIRALLING) {
                    // Execute action function for state one : event one
                  NextState = Rest;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  Event2Post.EventType = VALID_COLOR_FOUND;
                  // Posting spiralling event to DriveSM
                  PostMasterSM(Event2Post);
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                   printf("Finished moving. Hopefully on valid tile now.\r\n");
                  break;
                 }
                 
                // repeat cases as required for relevant events
            }
         }
         break;
      // repeat state pattern as required for other states
    }
    //   If we are making a state transition
    if (MakeTransition == true)
    {
       //   Execute exit function for current state
       CurrentEvent.EventType = ES_EXIT;
       RunPlanningSM(CurrentEvent);

       CurrentState = NextState; //Modify state variable

       //   Execute entry function for new state
       // this defaults to ES_ENTRY
       RunPlanningSM(EntryEventKind);
     }
     return(ReturnEvent);
}
/****************************************************************************
 Function
     StartPlanningSM

 Parameters
     None

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 2/18/99, 10:38AM
****************************************************************************/
void StartPlanningSM ( ES_Event_t CurrentEvent )
{
   // to implement entry to a history state or directly to a substate
   // you can modify the initialization of the CurrentState variable
   // otherwise just start in the entry state every time the state machine
   // is started
//   if ( ES_ENTRY_HISTORY != CurrentEvent.EventType )
//   {
        CurrentState = Rest;
//   }
   // call the entry function (if any) for the ENTRY_STATE
   RunPlanningSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryPlanningSM

 Parameters
     None

 Returns
     PlanningState_t The current state of the Planning state machine

 Description
     returns the current state of the Planning state machine
 Notes

 Author
     J. Edward Carryer, 2/11/05, 10:38AM
****************************************************************************/
PlanningState_t QueryPlanningSM ( void )
{
   return(CurrentState);
}

/***************************************************************************
 private functions
 ***************************************************************************/

static ES_Event_t DuringRest( ES_Event_t Event)
{
  ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption
  ES_Event_t Event2Post;

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if (Event.EventType == ES_ENTRY)
        
    {
      Event2Post.EventType = ES_MASTER_STOP;
      PostDriveSM(Event2Post);
        // implement any entry actions required for this state machine
        
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

static ES_Event_t DuringExploring( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if (Event.EventType == ES_ENTRY)
        
    {
        // implement any entry actions required for this state machine
        
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}


/***********************************************************************************/
void setMakeDecisionFlag(bool flag){
  makeDecision = flag;
}

bool CheckMakeDecision(void) {
  if (makeDecision){
    printf("Making decision\r\n");
    // check if robot should move
    bool shouldRobotMove = false;
    TeamObject_t OurTeamObject = getOurTeamObject();
    TeamObject_t OppTeamObject = getOppTeamObject();
    
    // loop through all miners
    for (int i = 0; i <2; i++) {
      // check if current miner is not in one of our permits
      bool inOurPermit = false;
      uint8_t currMinerTileID = OurTeamObject.minerArray[i].currentLoc.tileColor.colorID;
      printf("Miner idx %d is in tile %d\r\n", i , currMinerTileID);
      for (int j = 0; j < 3; j++) {
        // it is so skip this one
        if (OurTeamObject.permitLocs[j].tileColor.colorID == currMinerTileID) {
          inOurPermit = true;
          break;
        }
        
        }
      if (!inOurPermit){
          ES_Event_t E2P;
          E2P.EventType = MAKE_DECISION;
          PostMasterSM(E2P);
          makeDecision = false;
          return true;
      }
    }
    return false;
  }
  return false;
}
