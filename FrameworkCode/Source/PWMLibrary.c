/****************************************************************************
 Module
   PWMLibrary.c

 Revision
   1.0.1

 Description
   This is the PWM library file. Initializes pairs of PWM channels.

 Notes
 Channels used:
 Channel 0: PB6 (Group 0)
 Channel 1: PB7 (Group 0) 
 Channel 2: PB4 (Group 1)
 Channel 3: PB5 (Group 1)
 

 In all functions, duty is a number between 0 and 100.
 Period is always in microseconds.
 
 Functions useful:
 InitPWMLibrary(num_channels, period_group0, duty0, duty1, period_group1, duty2, duty3)
 void SetPeriod(uint32_t PeriodMicroS, uint8_t group)
 void SetDuty(uint32_t duty, uint8_t channel)
 
 History
 When           Who     What/Why
 -------------- ---     --------
 02/04/20 19:50 pvk      Creating a library useful for Lab 8 and project
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_pwm.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "bitdefs.h"


/*----------------------------- Module Defines ----------------------------*/
// 40,000 ticks per mS assumes a 40Mhz clock, we will use SysClk/32 for PWM
#define PWMTicksPerMicroS 40/32
// set 200 Hz frequency so 5mS period
#define PeriodInMicroS 170
// program generator A to go to 1 at rising comare A, 0 on falling compare A
#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
// program generator B to go to 1 at rising comare B, 0 on falling compare B
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )
#define BitsPerNibble 4
// we will use PWM module 0 for this demo and program it for up/down counting
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void Set0DC(uint8_t channel);
static void Set100DC(uint8_t channel);
static void RestoreDC(uint8_t channel);
void SetPeriod(uint32_t PeriodMicroS, uint8_t group);
void SetDuty(uint32_t duty, uint8_t channel);

/*---------------------------- Module Variables ---------------------------*/
// Arrays holding the necessary addresses

static const uint32_t PWM_Gen_Offset[4] = { 
  PWM_O_0_GENA, PWM_O_0_GENB, PWM_O_1_GENA, PWM_O_1_GENB};

static const uint32_t PWM_Load_Offset[2] = {
  PWM_O_0_LOAD, PWM_O_1_LOAD};

static const uint32_t PWM_Gen_Normal_Offset[4] = {
  GenA_Normal, GenB_Normal, GenA_Normal, GenB_Normal};
  
static const uint32_t PWM_CMP_Offset[4] = { 
  PWM_O_0_CMPA, PWM_O_0_CMPB, PWM_O_1_CMPA, PWM_O_1_CMPB};

static const uint32_t PWM_Gen_ACT_Zero[4] = {
  PWM_0_GENA_ACTZERO_ZERO, PWM_0_GENB_ACTZERO_ZERO, PWM_1_GENA_ACTZERO_ZERO, PWM_1_GENB_ACTZERO_ZERO};
  
static const uint32_t PWM_Gen_ACT_HUNDRED[4] = {
  PWM_0_GENA_ACTZERO_ONE, PWM_0_GENB_ACTZERO_ONE, PWM_1_GENA_ACTZERO_ONE, PWM_1_GENB_ACTZERO_ONE};

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitPWMLibrary

 Parameters
     See top of file

 Returns
     void

 Description
     Initializes channels for PWM (PB6, PB7, PB4, PB5)
 Notes

 Author
     Pranav Keni, 02/04/20, 20:00
****************************************************************************/
void InitPWMLibrary(uint8_t num, uint32_t period0, uint32_t duty0, uint32_t duty1, uint32_t period1, uint32_t duty2, uint32_t duty3 )
{
  
  // start by enabling the clock to the PWM Module (PWM0)
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  
  // enable the clock to Port B 
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  
  // Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
    (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);
  
  // make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
    ;
  
  //Initializing group 0 channels
  SetPeriod(period0, 0);
  SetDuty(duty0, 0);
  SetDuty(duty1, 1);
  
  if (num <= 2){
    // disable the PWM while initializing
    HWREG( PWM0_BASE+PWM_O_0_CTL ) = 0;
    // now configure the Port B pins to be PWM outputs
    // start by selecting the alternate function for PB6 & 7
    HWREG(GPIO_PORTB_BASE+GPIO_O_AFSEL) |= (BIT7HI | BIT6HI);
    
    // enable the PWM outputs
    HWREG( PWM0_BASE+PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM0EN);
    
  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 6 & 7
    HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) =
    (HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) & 0x00ffffff) + (4<<(7*BitsPerNibble)) +
    (4<<(6*BitsPerNibble));
    
    // Enable pins 6 & 7 on Port B for digital I/O
    HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT7HI | BIT6HI);
    
    // make pins 6 & 7 on Port B into outputs
    HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT7HI | BIT6HI);
  
  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
    HWREG(PWM0_BASE+ PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  }
  else
  {
    // disable the PWM while initializing
    HWREG( PWM0_BASE+PWM_O_0_CTL ) = 0;
    HWREG( PWM0_BASE+PWM_O_1_CTL ) = 0;
    
    //Initializing group 1 channels
    SetPeriod(period1, 1);
    SetDuty(duty2, 2);
    SetDuty(duty3, 3);
    
    // now configure the Port B pins to be PWM outputs
    // start by selecting the alternate function for PB6, PB7, PB4, PB5
    HWREG(GPIO_PORTB_BASE+GPIO_O_AFSEL) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);
    
    // enable the PWM outputs
    HWREG( PWM0_BASE+PWM_O_ENABLE) |= (PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM3EN | PWM_ENABLE_PWM2EN);
    
    // now choose to map PWM to those pins, this is a mux value of 4 that we
    // want to use for specifying the function on bits 6, 7, 4 & 5.
    HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) =
    (HWREG(GPIO_PORTB_BASE+GPIO_O_PCTL) & 0x00ffffff) + (4<<(7*BitsPerNibble)) +
    (4<<(6*BitsPerNibble)) + (4<<(5*BitsPerNibble)) + (4<<(4*BitsPerNibble));
    
    // Enable pins 6, 7, 5 & 4 on Port B for digital I/O
    HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);
    
    // make pins 6, 7, 5 & 4 on Port B into outputs
    HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= (BIT7HI |BIT6HI | BIT5HI | BIT4HI );
    
    // set the up/down count mode, enable the PWM generator and make
    // both generator updates locally synchronized to zero count
    HWREG(PWM0_BASE+ PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
    PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
    HWREG(PWM0_BASE+ PWM_O_1_CTL) = (PWM_1_CTL_MODE | PWM_1_CTL_ENABLE |
    PWM_1_CTL_GENAUPD_LS | PWM_1_CTL_GENBUPD_LS);
  } 
}
  

/***************************************************************************
Private functions
****************************************************************************/
static void Set0DC(uint8_t channel){
  // To program 0% DC, simply set the action on Zero to set the output to zero
  HWREG( PWM0_BASE+PWM_Gen_Offset[channel]) = PWM_Gen_ACT_Zero[channel];
  // don't forget to restore the proper actions when the DC drops below 100%
  // or rises above 0%
}

static void Set100DC(uint8_t channel){
  // To program 100% DC, simply set the action on Zero to set the output to one
  HWREG( PWM0_BASE+PWM_Gen_Offset[channel]) = PWM_Gen_ACT_HUNDRED[channel];

  // don't forget to restore the proper actions when the DC drops below 100%
  // or rises above 0%
}

static void RestoreDC(uint8_t channel){
// To restore the previos DC, simply set the action back to the normal actions
 HWREG( PWM0_BASE+PWM_Gen_Offset[channel]) = PWM_Gen_Normal_Offset[channel];
}

/***************************************************************************
Public functions
****************************************************************************/

void SetPeriod(uint32_t PeriodMicroS, uint8_t group){
  HWREG( PWM0_BASE+PWM_Load_Offset[group]) = ((PeriodMicroS * PWMTicksPerMicroS))>>1;
}


void SetDuty(uint32_t duty, uint8_t channel){
  
  if (duty <1){
      Set0DC(channel);
    }
    else if (duty >99){
      Set100DC(channel);
    }
    else{
      RestoreDC(channel);
    }
    HWREG( PWM0_BASE+PWM_CMP_Offset[channel]) = (uint32_t)HWREG(PWM0_BASE+PWM_Load_Offset[channel/2])*(1 - duty/100.0f);
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

