/****************************************************************************
 Module
   AccelSPIFSM.c

 Revision
   1.0.1

 Description
   This is a AccelSPI file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunAccelSPISM()
 10/23/11 18:20 jec      began conversion from SMAccelSPI.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PINDEFS.h" // update this 
#include "AccelSPIFSM.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "bitdefs.h"


// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/
#define AccelRequestTime 20

#define BW_RATE_ADDRESS 0x2C
#define BW_RATE_BYTE 0x0A //Data rate set at 100Hz
//#define BW_RATE_BYTE 0x08 //Data rate set at 25Hz
#define DATA_FORMAT_ADDRESS 0x31
#define DATA_FORMAT_BYTE 0x00 //bit6 low for SPI 4 wire, bit3 low for FULL_RES
#define POWER_CTL_ADDRESS 0x2D
#define POWER_CTL_BYTE 0x08  //measure bit high to set in measure mode

// Addition for Tap detection
#define INT_MAP_ADDRESS 0x2F
#define INT_MAP_BYTE BIT6LO   //Setting on SINLGE_TAP to be on INT1

#define THRESH_TAP_ADDRESS 0x1D
#define THRESH_TAP_BYTE 0x01  //Need to tune this (62.5mg per LSB)

#define DUR_ADDRESS 0x21
#define DUR_BYTE 0xFF     //Need to tune this (625us per LSB)

#define INT_ENABLE_ADDRESS 0x2E
#define INT_ENABLE_BYTE BIT6HI    //Activating only SINGLE_TAP interrupt

//  Register to clear source of interrupt (READ ONLY)
#define INT_SOURCE_ADDRESS 0x30

#define DUMMY_VARIABLE 0x00
#define ACCEL_DATA_ADDRESS (0x32 | BIT6HI | BIT7HI)  //Address of the first data register

#define COLLISION_THRESHOLD -50

#define MIN_ACCX -14
#define MAX_ACCX 5

#define SAMPLING_SIZE 40
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
void InitTivaAccelSPI(void);
void InitAccelParams(void);
void InitTivaInterrupt(void);
void WriteInitCommand(uint8_t address, uint8_t byte);
void RequestAccelData(void);
int16_t GetAccelX(void);
int16_t GetAccelY(void);
int16_t GetAccelZ(void);
uint8_t GetMinDiffIndex(int16_t* A , int val);
void SetGetHeading(bool flag);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static AccelSPIState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static uint8_t InitAddresses[3] = {BW_RATE_ADDRESS, DATA_FORMAT_ADDRESS, POWER_CTL_ADDRESS}; //,
                                  //INT_MAP_ADDRESS, THRESH_TAP_ADDRESS, DUR_ADDRESS, INT_ENABLE_ADDRESS }; 
static uint8_t InitDataBytes[3] = {BW_RATE_BYTE, DATA_FORMAT_BYTE, POWER_CTL_BYTE}; // ,
                                  // INT_MAP_BYTE, THRESH_TAP_BYTE, DUR_BYTE, INT_ENABLE_BYTE };
static bool HasInitialized, GetHeadingFlag;
static volatile uint8_t device_ID;
static uint16_t X_LOBYTE, X_HIBYTE, Y_LOBYTE, Y_HIBYTE, Z_LOBYTE, Z_HIBYTE;
static int16_t X_offset, Y_offset, Z_offset;

static int16_t ax, index;
static int16_t ay;

static int16_t ax_avg, ax_sum;
static int16_t ay_avg, ay_sum;
static int16_t avg_count;
static int16_t MovingAvgArray[SAMPLING_SIZE][2];

// Emperical Heading Data
static int16_t yValFor_x_neg14[] = {-19, -17, -15, -13};
static int16_t yValFor_x_neg13[] = {-21, -11, -9};
static int16_t yValFor_x_neg12[] = {-24, -23, -22, -8};
static int16_t yValFor_x_neg11[] = {-24, -7};
static int16_t yValFor_x_neg10[] = {-24, -6};
static int16_t yValFor_x_neg9[] = {-25, -5};
static int16_t yValFor_x_neg8[] = {-25, -5};
static int16_t yValFor_x_neg7[] = {-26, -5};
static int16_t yValFor_x_neg6[] = {-26, -5};
static int16_t yValFor_x_neg5[] = {-26, -5};
static int16_t yValFor_x_neg4[] = {-27, -4};
static int16_t yValFor_x_neg3[] = {-27, -4};
static int16_t yValFor_x_neg2[] = {-27, -4};
static int16_t yValFor_x_neg1[] = {-26, -4};
static int16_t yValFor_x_zero[] = {-26, -6, -5};
static int16_t yValFor_x_pos1[] = {-25, -24, -7};
static int16_t yValFor_x_pos2[] = {-24, -23, -9, -8};
static int16_t yValFor_x_pos3[] = {-22, -20, -17, -16, -15, -14, -13, -12, -11};	
static int16_t yValFor_x_pos4[] = {-21, -20, -19, -17, -15, -13};
static int16_t yValFor_x_pos5[] = {-19, -17};

static uint16_t HeadingFor_x_neg14[] = {200, 190, 180, 170};
static uint16_t HeadingFor_x_neg13[] = {210, 160, 150};
static uint16_t HeadingFor_x_neg12[] = {240, 230, 220, 140};
static uint16_t HeadingFor_x_neg11[] = {243, 130};
static uint16_t HeadingFor_x_neg10[] = {246, 120};
static uint16_t HeadingFor_x_neg9[] = {250, 115};
static uint16_t HeadingFor_x_neg8[] = {255, 110};
static uint16_t HeadingFor_x_neg7[] = {260, 105};
static uint16_t HeadingFor_x_neg6[] = {263, 100};
static uint16_t HeadingFor_x_neg5[] = {266, 98};
static uint16_t HeadingFor_x_neg4[] = {270, 95};
static uint16_t HeadingFor_x_neg3[] = {275, 92};
static uint16_t HeadingFor_x_neg2[] = {280, 90};
static uint16_t HeadingFor_x_neg1[] = {285, 85};
static uint16_t HeadingFor_x_zero[] = {290, 70, 80};
static uint16_t HeadingFor_x_pos1[] = {300, 310, 60};
static uint16_t HeadingFor_x_pos2[] = {320, 330, 40, 50};
static uint16_t HeadingFor_x_pos3[] = {340, 345, 0, 5, 10, 15, 20, 25, 30};
static uint16_t HeadingFor_x_pos4[] = {343, 346, 350, 0, 10, 20};
static uint16_t HeadingFor_x_pos5[] = {350, 0};
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitAccelSPIFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitAccelSPIFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  HasInitialized = false;
  GetHeadingFlag = false;
  //initialize the TIVA SSI3 module
  InitTivaAccelSPI();
//  InitTivaInterrupt();
  printf("Tiva module intialized \r\n");
	
	X_offset = 0;
	Y_offset = 0;
  
  // put us into the Initial PseudoState
  CurrentState = InitAccelSPIState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostAccelSPIFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostAccelSPIFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunAccelSPIFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunAccelSPIFSM(ES_Event_t ThisEvent)
{
  static uint8_t init_count = 0;
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  ES_Event_t Event2Post;
  
//  if (ThisEvent.EventType == COLLISION_EVENT){
//    printf("Collision detected by accel\r\n");
//  }
  switch (CurrentState)
  {
    case InitAccelSPIState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
//        CurrentState = Initializing;
        CurrentState = Initializing;
        Event2Post.EventType = ES_ACCEL_READY2WRITE;
        PostAccelSPIFSM(Event2Post);
      }
    }
    break;

    case Initializing:        // If current state is state one
    {
      if (ThisEvent.EventType == ES_ACCEL_READY2WRITE){
        CurrentState = Waiting;
        /* Sequence of initialization steps
        1. Setting the data transfer frequency
        2. Writing to the data format register to enable 4 wire SPI mode and 10 bit resolution
        3. Writing to the POWER_CTL register to set measure bit HI
        4. Writing to map interrupt to INT0 pin
        5. Setting the TAP_THRESHOLD value
        6. Setting the duration value
        7. Enable the interrupt
        */
        WriteInitCommand(InitAddresses[init_count], InitDataBytes[init_count]);
        //WriteInitCommand(0x80, 0x00);
        ES_Timer_InitTimer(AccelTimer , AccelRequestTime);
        init_count++;
        printf("Init count is %u\r\n", init_count);
        if (init_count == sizeof(InitAddresses)){
          HasInitialized = true;
          printf("Done with accel initialization \r\n");
        }
      } 
    }
    break;
    
    case Waiting:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == AccelTimer){
        if (HasInitialized){
          CurrentState = Active;
//          printf("X accel is %d", GetAccelX());
//          printf("\tY accel is %d\r\n", GetAccelY());
//					 printf("\t Z accel is %d\r\n ", GetAccelZ());
          ax = GetAccelX();
          ay = GetAccelY();
					if (index == SAMPLING_SIZE){
						index = 0;
					}
					MovingAvgArray[index][0] = ax;
					MovingAvgArray[index][1] = ay;
					index++;
//          if (GetHeadingFlag){
//            printf("Counting \r\n");
//            ax_sum += ax;
//            ay_sum += ay;
//            avg_count++;
//            ax_avg = ax_sum/avg_count;
//            ay_avg = ay_sum/avg_count;
//          }
//          else{
//            
//          }
          
//					printf("Magnitude of accel is %d\r\n", (GetAccelX()*GetAccelX() + GetAccelY()*GetAccelY() + GetAccelZ()*GetAccelZ())); 
//					printf("\r\n");
//          printf("Z accel is %d\r\n ", GetAccelZ());
//          if (GetAccelX() < COLLISION_THRESHOLD){
//            printf("Collision detected \r\n");
//          }
//          printf("Device ID is %x\r\n" , device_ID);
        }
        else{
          CurrentState = Initializing;
        }
        Event2Post.EventType = ES_ACCEL_READY2WRITE;
        PostAccelSPIFSM(Event2Post);
      }        
    }
    break;
    
    case Active:
    {
      if (ThisEvent.EventType == ES_ACCEL_READY2WRITE){
        CurrentState = Waiting;
        RequestAccelData(); 
        ES_Timer_InitTimer(AccelTimer , AccelRequestTime);
      } 
    }
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryAccelSPISM

 Parameters
     None

 Returns
     AccelSPIState_t The current state of the AccelSPI state machine

 Description
     returns the current state of the AccelSPI state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
AccelSPIState_t QueryAccelSPIFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
void InitTivaAccelSPI(void){
  //  1. Enable the clock to the GPIO port 
  // Port D by default is SSI power on default 
  // PD0-3
  HWREG(SYSCTL_RCGCGPIO) |= (ACTIVATE_PORT_D);
  
  //  2. Enable the clock to SSI module 3
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R3;
  
  //  3. Wait for the GPIO port to be ready
  while ((HWREG(TOGGLE_PORT) & ACTIVATE_PORT_D) != ACTIVATE_PORT_D) {
  }
  
  //  4. Program the GPIO to use the alternate functions on the SSI pins
  // unnecessary since this is default for PD0-3
  HWREG(GPIO_PORTD_BASE+GPIO_O_AFSEL) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
  //  5. Set mux position in GPIOPCTL to select the SSI use of the pins 
  uint8_t mux_val = 1; 
  // the  value being multiplied by bits per nibble is the pin on the port A 
  
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) & 0xffff0000) +
    (mux_val<<(0*BITS_PER_NYBBLE)) + (mux_val<<(1*BITS_PER_NYBBLE)) + 
      (mux_val<<(2*BITS_PER_NYBBLE)) + (mux_val<<(3*BITS_PER_NYBBLE));

  //  6. Program the port lines for digital I/O
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
  
  //  7. Program the required data directions on the port lines
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT3HI);
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= (BIT2LO);
  
  //  8. If using SPI mode 3, program the pull-up on the clock line
  // enable the PUR on PD0 which is SSI3 SCK
  HWREG(GPIO_PORTD_BASE + GPIO_O_PUR) |= BIT0HI; // enable pull up on SSI3 SCK
  
  //  9. Wait for the SSI3 to be ready
  while((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R3) != SYSCTL_RCGCSSI_R3){
  }
  
  //  10. Make sure that the SSI is disabled before programming mode bits
  HWREG(SSI3_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;
  
  //  11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  HWREG(SSI3_BASE + SSI_O_CR1) &= ~SSI_CR1_MS;
  HWREG(SSI3_BASE + SSI_O_CR1) |= SSI_CR1_EOT;
  
  //  12. Configure the SSI clock source to the system clock
  HWREG(SSI3_BASE + SSI_O_CC) &= SSI_CC_CS_SYSPLL; 
  
  //  13. Configure the clock pre-scaler
  // target freq: 500,000 HZ
  // step 1) set divisor = 4
  // step 2) solve for SCR (prescaler) and if it's in range,
  // shift it in the right location
  HWREG(SSI3_BASE + SSI_O_CPSR) = 4;
  HWREG(SSI3_BASE + SSI_O_CR0) = (HWREG(SSI3_BASE + SSI_O_CR0) & ~SSI_CR0_SCR_M) |
    (19 << SSI_CR0_SCR_S);
  
  
  //  14. Configure clock rate (SCR), phase & polarity (SPH, SPO), mode (FRF), data size (DSS)
  // for PCB generator (timing diagram)
  // Phase is 1 because writing data happens on even edges
  // Polarity is 1 because serial clock idles high
  HWREG(SSI3_BASE + SSI_O_CR0) |= SSI_CR0_SPH; // this is phase 1
  HWREG(SSI3_BASE + SSI_O_CR0) |= SSI_CR0_SPO; // this is polarity 1
  HWREG(SSI3_BASE + SSI_O_CR0) &= ~SSI_CR0_FRF_M; 
  HWREG(SSI3_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8; 
  
  //  15. Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI3_BASE + SSI_O_IM) |= SSI_IM_TXIM;

  //  16. Make sure that the SSI is enabled for operation
  HWREG(SSI3_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
  //HWREG(SSI3_BASE + SSI_O_DR) = RequestCommandByte;
  //  17. Enable the NVIC interrupt for the SSI3 when starting to transmit
  //HWREG(NVIC_EN1) = BIT26HI;  //Interrupt number 58 for SSI3
  
    // make sure interrupts are enabled globally
  __enable_irq();
  printf("Finished TIVA init \r\n");
}



void WriteInitCommand(uint8_t address, uint8_t byte){
  HWREG(SSI3_BASE + SSI_O_DR) = address;
  HWREG(SSI3_BASE + SSI_O_DR) = byte;
  //  Enable the NVIC interrupt for the SSI3 when starting to transmit
  HWREG(NVIC_EN1) = BIT26HI;  //Interrupt number 58 for SSI3
//  HWREG(NVIC_EN1) = BIT2HI;  //Interrupt number 34 for SSI3
}

void RequestAccelData(void){
//  HWREG(SSI3_BASE + SSI_O_DR) = 0x80;
//  HWREG(SSI3_BASE + SSI_O_DR) = 0x00;
  HWREG(SSI3_BASE + SSI_O_DR) = ACCEL_DATA_ADDRESS;

  for (int i=0; i<6; i++){
    HWREG(SSI3_BASE + SSI_O_DR) = DUMMY_VARIABLE;
  }
  HWREG(NVIC_EN1) = BIT26HI;  //Interrupt number 58 for SSI3
//  HWREG(NVIC_EN1) = BIT2HI;  //Interrupt number 34 for SSI3
}

void AccelInterrupt(void){
  // Disable interrupt
  HWREG(NVIC_DIS1) = BIT26HI;
//  HWREG(NVIC_DIS1) = BIT2HI;
  
  if (HasInitialized){
    HWREG(SSI3_BASE + SSI_O_DR);
//    device_ID = HWREG(SSI3_BASE + SSI_O_DR);
    X_LOBYTE = HWREG(SSI3_BASE + SSI_O_DR);
    X_HIBYTE = HWREG(SSI3_BASE + SSI_O_DR) << 8;
    Y_LOBYTE = HWREG(SSI3_BASE + SSI_O_DR);
    Y_HIBYTE = HWREG(SSI3_BASE + SSI_O_DR) << 8;
    Z_LOBYTE = HWREG(SSI3_BASE + SSI_O_DR);
    Z_HIBYTE = HWREG(SSI3_BASE + SSI_O_DR) << 8;
  }
  else{
    HWREG(SSI3_BASE + SSI_O_DR);
    HWREG(SSI3_BASE + SSI_O_DR);
  }
}

int16_t GetAccelX(void){
  if (X_HIBYTE & BIT9HI){
    return ((X_HIBYTE | X_LOBYTE | 0xFC00) - X_offset);
  }
  else{
    return (((X_HIBYTE | X_LOBYTE) & ~0xFC00) - X_offset);
  }
}

int16_t GetAccelY(void){
  if (Y_HIBYTE & BIT9HI){
    return ((Y_HIBYTE | Y_LOBYTE | 0xFC00) - Y_offset);
  }
  else{
    return (((Y_HIBYTE | Y_LOBYTE) & ~0xFC00) - Y_offset);
  }
}

int16_t GetAccelZ(void){
  if (Z_HIBYTE & BIT9HI){
    return (Z_HIBYTE | Z_LOBYTE | 0xFC00);
  }
  else{
    return ((Z_HIBYTE | Z_LOBYTE) & ~0xFC00);
  }
}


uint16_t GetCurrentHeading(void){
//	int16_t ax = GetAccelX();
//	int16_t ay = GetAccelY();
	uint8_t y_ind;
	uint16_t CurrHeading;
  
  ax_sum = 0;
  ay_sum = 0;
	
	for (int i = 0 ; i< SAMPLING_SIZE; i++){
		ax_sum += MovingAvgArray[i][0];
		ay_sum += MovingAvgArray[i][1];
	}
	ax_avg = ax_sum/SAMPLING_SIZE;
	ay_avg = ay_sum/SAMPLING_SIZE;
	
	if (ax_avg < MIN_ACCX){
		ax_avg = MIN_ACCX;
	}
	else if (ax_avg > MAX_ACCX){
		ax_avg = MAX_ACCX;
	}
	
//	printf("Sum ax = %d\r\n", ax_sum);
//	printf("Sum ay = %d\r\n", ay_sum);
//	
//	printf("Avg ax = %d\r\n", ax_avg);
//	printf("Avg ay = %d\r\n", ay_avg);
	
	uint8_t min_idx = 0;
	uint8_t minDiff = 255;
	
	switch (ax_avg)
	{
		case -14:
		{
			minDiff = 255;
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg14) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg14[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg14[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
			// y_ind = GetMinDiffIndex(yValFor_x_neg14, ay_avg);
			CurrHeading = HeadingFor_x_neg14[y_ind];
			
			
		}
		break;
		
		case -13:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg13) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg13[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg13[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
			// y_ind = GetMinDiffIndex(yValFor_x_neg13, ay_avg);
			CurrHeading = HeadingFor_x_neg13[y_ind];
		}
		break;
		
		case -12:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg12) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg12[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg12[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_neg12, ay_avg);
			CurrHeading = HeadingFor_x_neg12[y_ind];
		}
		break;
		
		case -11:
		{
			for (uint8_t i = 0; i < (sizeof(1) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg11[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg11[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_neg11, ay_avg);
			CurrHeading = HeadingFor_x_neg11[y_ind];
		}
		break;
		
		case -10:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg10) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg10[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg10[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_neg10, ay_avg);
			CurrHeading = HeadingFor_x_neg10[y_ind];
		}
		break;
		
		case -9:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg9) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg9[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg9[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_neg9, ay_avg);
			CurrHeading = HeadingFor_x_neg9[y_ind];
		}
		break;
		
		case -8:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg8) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg8[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg8[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_neg8, ay_avg);
			CurrHeading = HeadingFor_x_neg8[y_ind];
		}
		break;
		
		case -7:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg7) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg7[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg7[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_neg7, ay_avg);
			CurrHeading = HeadingFor_x_neg7[y_ind];
		}
		break;
		
		case -6:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg6) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg6[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg6[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_neg6, ay_avg);
			CurrHeading = HeadingFor_x_neg6[y_ind];
		}
		break;
		
		case -5:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg5) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg5[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg5[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_neg5, ay_avg);
			CurrHeading = HeadingFor_x_neg5[y_ind];
		}
		break;
		
		case -4:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg4) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg4[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg4[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_neg4, ay_avg);
			CurrHeading = HeadingFor_x_neg4[y_ind];
		}
		break;
		
		case -3:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg3) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg3[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg3[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_neg3, ay_avg);
			CurrHeading = HeadingFor_x_neg3[y_ind];
		}
		break;
		
		case -2:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg2) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg2[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg2[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_neg2, ay_avg);
			CurrHeading = HeadingFor_x_neg2[y_ind];
		}
		break;
		
		case -1:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_neg1) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_neg1[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_neg1[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_neg1, ay_avg);
			CurrHeading = HeadingFor_x_neg1[y_ind];
		}
		break;
		
		case 0:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_zero) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_zero[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_zero[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_zero, ay_avg);
			CurrHeading = HeadingFor_x_zero[y_ind];
		}
		break;
		
		case 1:
		{
//			printf("Got here\r\n");
			for (uint8_t i = 0; i < (sizeof(yValFor_x_pos1) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_pos1[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_pos1[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
//			y_ind = GetMinDiffIndex(yValFor_x_pos1, ay_avg);
			CurrHeading = HeadingFor_x_pos1[y_ind];
		}
		break;
		
		case 2:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_pos2) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_pos2[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_pos2[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_pos2, ay_avg);
			CurrHeading = HeadingFor_x_pos2[y_ind];
		}
		break;
		
		case 3:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_pos3) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_pos3[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_pos3[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_pos3, ay_avg);
			CurrHeading = HeadingFor_x_pos3[y_ind];
		}
		break;
		
		case 4:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_pos4) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_pos4[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_pos4[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_pos4, ay_avg);
			CurrHeading = HeadingFor_x_pos4[y_ind];
		}
		break;
		
		case 5:
		{
			for (uint8_t i = 0; i < (sizeof(yValFor_x_pos5) / sizeof(int16_t)); i++){
//				printf("Min diff = %d\r\n", minDiff);
			if (abs(yValFor_x_pos5[i] - ay_avg) <= minDiff){
				minDiff = abs(yValFor_x_pos5[i] - ay_avg);
				min_idx = i;
			}
		}
			y_ind = min_idx;
		
//			y_ind = GetMinDiffIndex(yValFor_x_pos5, ay_avg);
			CurrHeading = HeadingFor_x_pos5[y_ind];
		}
		break;
		
	}
//	ax_avg = 0;
//  ax_sum = 0;
//  ay_sum =0;
//  ay_avg = 0;
//  avg_count = 0;
	printf("Current heading: %d\r\n", CurrHeading);
	return CurrHeading;

}

//uint8_t GetMinDiffIndex(int16_t* A , int val){
//	uint8_t min_idx = 0;
//	uint8_t minDiff = 255;
//	uint8_t len;
//	int16_t vec = *A;
//	len = (sizeof(A) / sizeof(int16_t));
//	
//	printf("Size of A: %d\r\n", sizeof(vec));
//	
//	printf("Size of int16: %d\r\n", sizeof(int16_t));
//	
//	for (uint8_t i = 0; i < (sizeof(A) / sizeof(int16_t)); i++){
//		printf("Min diff = %d\r\n", minDiff);
//		if (abs(A[i] - val) <= minDiff){
//			minDiff = abs(A[i] - val);
//			min_idx = i;
//		}
//	}		
//	return min_idx;
//}

//void SetGetHeading(bool flag){
//  GetHeadingFlag = flag;
//}
