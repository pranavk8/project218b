/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "StepService1.h"
#include "PINDEFS.h"
#include "ADService.h"
#include "PWM16Tiva.h"

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <float.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static int8_t stepCount;
static int8_t stride; 
static bool stepDirection = true;

static uint8_t stepArray[16][4] = {
  {71, 0, 71, 0},
  {92, 0, 38, 9},
  {100, 0, 0, 0},
  {92, 0, 0, 38},
  {71, 0, 0, 71},
  {38, 0, 0, 92},
  {0, 0, 0, 100},
  {0, 38, 0, 92},
  {0, 71, 0, 71},
  {0, 92, 0, 38},
  {0, 100, 0, 0},
  {0, 92, 38, 0},
  {0, 71, 71, 0},
  {0, 38, 92, 0},
  {0, 0, 100, 0},
  {38, 0, 92, 0}
};

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitStepService1(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************/
  // initialize ports
  HWREG(SYSCTL_RCGCGPIO) |= (ACTIVATE_PORT_B);
  while ((HWREG(TOGGLE_PORT) & (ACTIVATE_PORT_B)) != (ACTIVATE_PORT_B )) {
  }
  printf("Activated B ports.\r\n");
  
  // activates the pins 0 AND 1 TYPICAL
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI); // hi is output 
  
  HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) |= (BIT0HI | BIT1HI);
  
  // ACTIVATE THE PWM PINS
  PWM_TIVA_Init(4);
  PWM_TIVA_SetFreq(500, 0);
  PWM_TIVA_SetFreq(500, 1);
  
  // start timers...? maybe not; timers started in ADService
  
  // change incremenet and initial step values depending
  // depending on the drive type
  stepCount = 0; // init step count
  stride = 1;
  
 // printf("\r\nDebugging.");
   /*******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostStepService1(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunStepService1(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************/
  
  uint16_t s_StepTimer = QueryStepTime();
  
  switch(ThisEvent.EventType)
  {
    case ES_INIT:
    {
      // printf("\r\nDebugging.");
    }
    break;
    
    case ES_TIMEOUT:
    {
      if(ThisEvent.EventParam == StepTimer) {
        // incrememebnt stepcount
        if (stepDirection == true){
          stepCount = stepCount + stride;
          if(stepCount > 15) stepCount = stepCount%16;
        } else if (stepDirection == false) {
          stepCount = stepCount - stride;
          if (stepCount < 0) stepCount = stepCount + 16;
        }
        // stepCount = stepCount + stepDirection * stride;
        
        // logic to ensure we don't overflow
        
        
        // printf("\r\n");
        for(int i = 0; i < 4; i++){
          // printf("%d ", stepArray[stepCount][i]);

          if (stepCount == 2 || stepCount == 10) {
            // enable 12
            HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) |= BIT0HI;
            // disable 34EN
            HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= BIT1LO;
          } else if (stepCount == 6 || stepCount == 14) {
            // disable 12EN
            HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) &= BIT0LO;
            // enable 34EN
            HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) |= BIT1HI;
          } else {
            // enable 12EN, and 34EN
            HWREG(GPIO_PORTB_BASE + GPIO_O_DATA + ALL_BITS) |= (BIT0HI | BIT1HI);
          }
          PWM_TIVA_SetDuty(stepArray[stepCount][i], i);
        }
        // printf("\tr:\t%d", s_StepTimer);
        //printf("\tStep Count:\t %d", stepCount);
        ES_Timer_InitTimer(StepTimer, s_StepTimer);
       // ES_Timer_InitTimer(StepTimer, 13);
        //printf("\r\n13");
      }
    }
    break;

    case ES_DIRECTION_CHANGE:
    {
      // change the step direction
      if(stepDirection == true) {
        stepDirection = false;
      } else {
        stepDirection = true;
      }
      printf("\r\nChange Direction");
    }
    break;
    
  }
   /*******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/



/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

