/****************************************************************************
 Module
   NormalOpsSM.c

 Revision
   2.0.1

 Description
   This is a template file for implementing state machines.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/27/17 09:48 jec      another correction to re-assign both CurrentEvent
                         and ReturnEvent to the result of the During function
                         this eliminates the need for the prior fix and allows
                         the during function to-remap an event that will be
                         processed at a higher level.
 02/20/17 10:14 jec      correction to Run function to correctly assign 
                         ReturnEvent in the situation where a lower level
                         machine consumed an event.
 02/03/16 12:38 jec      updated comments to reflect changes made in '14 & '15
                         converted unsigned char to bool where appropriate
                         spelling changes on true (was True) to match standard
                         removed local var used for debugger visibility in 'C32
                         commented out references to Start & RunLowerLevelSM so
                         that this can compile. 
 02/07/13 21:00 jec      corrections to return variable (should have been
                         ReturnEvent, not CurrentEvent) and several EV_xxx
                         event names that were left over from the old version
 02/08/12 09:56 jec      revisions for the Events and Services Framework Gen2
 02/13/10 14:29 jec      revised Start and run to add new kind of entry function
                         to make implementing history entry cleaner
 02/13/10 12:29 jec      added NewEvent local variable to During function and
                         comments about using either it or Event as the return
 02/11/10 15:54 jec      more revised comments, removing last comment in during
                         function that belongs in the run function
 02/09/10 17:21 jec      updated comments about internal transitions on During funtion
 02/18/09 10:14 jec      removed redundant call to RunLowerlevelSM in EV_Entry
                         processing in During function
 02/20/07 21:37 jec      converted to use enumerated type for events & states
 02/13/05 19:38 jec      added support for self-transitions, reworked
                         to eliminate repeated transition code
 02/11/05 16:54 jec      converted to implment hierarchy explicitly
 02/25/03 10:32 jec      converted to take a passed event parameter
 02/18/99 10:19 jec      built template from MasterMachine.c
 02/14/99 10:34 jec      Began Coding
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#include "PlanningSM.h"
#include "NavigatingSM.h"
#include "MiningOpsInfo.h"
#include "SpudSPI.h"
#include "SensorsService.h"
#include "ColorSensor.h"
#include "AccelSPIFSM.h"
#include "MasterSM.h"
#include "NormalOpsSM.h"
#include "DriveSM.h"
#include "Hardware.h"
/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE STATE_ZERO
#define PauseTime 2000

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/
static ES_Event_t DuringWaitingForPermits( ES_Event_t Event);
static ES_Event_t DuringPlanning( ES_Event_t Event);
static ES_Event_t DuringNavigating( ES_Event_t Event);
bool GetIfMinerIsPickedUp(void);
GridTile_t getTargetTile(void);
Miner_t getTargetMiner(void);
void SetMinerPickedUp(bool flag);
GridTile_t getCurrLoc(void);
void MakeDecision(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static NormalOpsState_t CurrentState;
static TeamObject_t OurTeamObject, OppTeamObject;
static bool isMinerPickedUp = false;
static GridTile_t currentLoc, targetTile;
static uint16_t currHeading;
static Miner_t targetMiner;
static uint8_t minerIdx;
static uint8_t targetIdx;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunNormalOpsSM

 Parameters
   ES_Event_t: the event to process

 Returns
   ES_Event_t: an event to return

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 2/11/05, 10:45AM
****************************************************************************/
ES_Event_t RunNormalOpsSM( ES_Event_t CurrentEvent )
{
   bool MakeTransition = false;/* are we making a state transition? */
   NormalOpsState_t NextState = CurrentState;
   ES_Event_t EntryEventKind = { ES_ENTRY, 0 };// default to normal entry to new state
   ES_Event_t ReturnEvent = CurrentEvent; // assume we are not consuming event

   if (CurrentEvent.EventType == ES_PERMITS_EXPIRED){
		 ES_Event_t E2P;
		 E2P.EventType = ES_MASTER_STOP;
		 PostDriveSM(E2P);
		 printf("Permits expired \r\n");
     CurrentEvent.EventType = ES_EXIT;
     RunNormalOpsSM(CurrentEvent);
		 CurrentState = WaitingForPermits;
     CurrentEvent.EventType = ES_ENTRY;
     RunNormalOpsSM(CurrentEvent);
   }
   
   switch ( CurrentState )
   {
       case WaitingForPermits :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringWaitingForPermits(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case ES_PERMITS_ISSUED : //If event is event one
                  // Execute action function for state one : event one
                  NextState = Planning;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  printf("Permits Issued. Next State: Planning\r\n");
                  setMiningOpsStatus(true);
                  break;
                // repeat cases as required for relevant events
            }
         }
       break;
         
       case Planning :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringPlanning(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case ES_GOAL_SET : //If event is event one
                  // Execute action function for state one : event one
                  NextState = Navigating;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
               
                  printf("Goal set. Next State: Navigating\r\n");
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
               
               case VALID_COLOR_FOUND : //If event is event one
                  // Execute action function for state one : event one
                  //Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                // repeat cases as required for relevant events
            }
         }
       break;
         
         
       case Navigating :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringNavigating(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case ES_TARGET_REACHED : //If event is event one
                  // Execute action function for state one : event one
                  NextState = Planning;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
							 printf("Going to planning\r\n");
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                // repeat cases as required for relevant events
							 
							 case ES_BEAM_RESTORED:
									SetMinerPickedUp(false);
									setCheckMinerFlag(false);
									NextState = Planning;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
									printf("Going to planning\r\n");
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
            }
         }
       break;  
      // repeat state pattern as required for other states
    }
    //   If we are making a state transition
    if (MakeTransition == true)
    {
       //   Execute exit function for current state
       CurrentEvent.EventType = ES_EXIT;
       RunNormalOpsSM(CurrentEvent);

       CurrentState = NextState; //Modify state variable

       //   Execute entry function for new state
       // this defaults to ES_ENTRY
       RunNormalOpsSM(EntryEventKind);
     }
     return(ReturnEvent);
}
/****************************************************************************
 Function
     StartNormalOpsSM

 Parameters
     None

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 2/18/99, 10:38AM
****************************************************************************/
void StartNormalOpsSM ( ES_Event_t CurrentEvent )
{
   // to implement entry to a history state or directly to a substate
   // you can modify the initialization of the CurrentState variable
   // otherwise just start in the entry state every time the state machine
   // is started
//   if ( ES_ENTRY_HISTORY != CurrentEvent.EventType )
//   {
        CurrentState = WaitingForPermits ;
			printf("Entering waiting for permits\r\n");
//   }
   // call the entry function (if any) for the ENTRY_STATE
   RunNormalOpsSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryNormalOpsSM

 Parameters
     None

 Returns
     NormalOpsState_t The current state of the NormalOps state machine

 Description
     returns the current state of the NormalOps state machine
 Notes

 Author
     J. Edward Carryer, 2/11/05, 10:38AM
****************************************************************************/
NormalOpsState_t QueryNormalOpsSM ( void )
{
   return(CurrentState);
}

/***************************************************************************
 private functions
 ***************************************************************************/

static ES_Event_t DuringWaitingForPermits( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY) // ||(Event.EventType == ES_ENTRY_HISTORY) )
    {
        // implement any entry actions required for this state machine
        
      /* TODO
      1. Get our miners
      2. Get our permit locations
      3. Reset permit status
      4. Turn OFF mining in progess LED
      */
      // get our miners and permits
      OurTeamObject = getOurTeamObject();
      OppTeamObject = getOppTeamObject();
      
      // reset permit status to read them
      resetStatus();
      
      // Mining in Progess OFF
      setMiningOpsStatus(false);
			
			CurrentState = WaitingForPermits;
      
      // At beginning we don't have a miner
			if(readDigitalInput(PORT_BEAM_BREAK, PIN_BEAM_BREAK) == 0){
				isMinerPickedUp = true;
			}
			else{
				isMinerPickedUp = false;
			}
      //  isMinerPickedUp = false;
      
      // after that start any lower level machines that run in this state
      //StartLowerLevelSM( Event );
      // repeat the StartxxxSM() functions for concurrent state machines
      // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}


static ES_Event_t DuringPlanning( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY) // ||(Event.EventType == ES_ENTRY_HISTORY) )
    {
        // implement any entry actions required for this state machine
        
      /* TODO
      1. State update (if returned value is UNKNOWN_COL then post INVALID_ROBOT_POS to master
      2. Set target location (posts ES_GOAL_SET to Master if all goes well
      */
      
      // State Update
      Color_t currentColor = getCurrentColor();
      currentLoc = getCurrentTile(currentColor);  // curr x, y in here
      currHeading = GetCurrentHeading();  // phi
      printf("Current color %d\r\n" , currentColor.colorID);
      printf("Current tile: %d\r\n" , currentLoc.tileColor.colorID);
      
      OurTeamObject = getOurTeamObject();
      OppTeamObject = getOppTeamObject();
      
      // if the current location is invalid, explore
      if (currentColor.colorID >= 16) {
        ES_Event_t Event2Post;
        Event2Post.EventType = INVALID_ROBOT_POS;
        PostMasterSM(Event2Post);
        printf("Not on a valid color. Will need to explore\r\n");
      }
      // Turn on decision maker
      else {
        setMakeDecisionFlag(true);
      }
      
      // after that start any lower level machines that run in this state
      StartPlanningSM(Event);
      // repeat the StartxxxSM() functions for concurrent state machines
      // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
      ReturnEvent = RunPlanningSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

static ES_Event_t DuringNavigating( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY) // ||(Event.EventType == ES_ENTRY_HISTORY) )
    {
        // implement any entry actions required for this state machine
        
      /* TODO
      1. Init Pause timer
      2. Turn beam break event checker ON
      */
      // Init pause timer
      ES_Timer_InitTimer(PauseTimer, PauseTime);
      printf("ES entry in DuringNavigating");
      // turn beam break event checker on
      setBeamBreak(true);
      StartNavigatingSM(ReturnEvent);
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        RunNavigatingSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
      ReturnEvent = RunNavigatingSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}


GridTile_t getTargetTile(void) {
  return OurTeamObject.permitLocs[targetIdx];
}

Miner_t getTargetMiner(void) {
  return OurTeamObject.minerArray[minerIdx];
}

bool GetIfMinerIsPickedUp(void){
  return isMinerPickedUp;
}

void SetMinerPickedUp(bool flag){
 isMinerPickedUp = flag;
}

GridTile_t getCurrLoc(void) {
  return currentLoc;
}

void MakeDecision(void){
  
  if (isMinerPickedUp) {
      // Look for unoccupied squares
      uint16_t minDist = 65535;
      targetIdx = 0;
      // Loop through miners and check which squares are unoccupied
      Miner_t allMiners[4] = {OurTeamObject.minerArray[0], OurTeamObject.minerArray[1], OppTeamObject.minerArray[0], OppTeamObject.minerArray[0]};
      // first loop through all permit locations
      for (int i = 0; i < 3; i++) {
        GridTile_t currPermit2Examine = OurTeamObject.permitLocs[i];
        bool isTileOccupied = false;
        // Loop through all miners
        printf("Current permit being examined %d\r\n", currPermit2Examine.tileColor.colorID);
        for (int j = 0; j < 4; j++) {
          if (currPermit2Examine.tileColor.colorID == allMiners[j].currentLoc.tileColor.colorID) {
            // occupied so skip this
            isTileOccupied = true;
            break;
          }
        }
        if (!isTileOccupied) {
          uint16_t dist = computeTileDistance(currentLoc, currPermit2Examine);
          printf("Distance computed is: %d\r\n", dist);
          if (dist < minDist) {
            minDist = dist;
            targetIdx = i;
        }
          
        }
        
      }
      
      
      targetTile = OurTeamObject.permitLocs[targetIdx];
      printf("\r\n");
      printf("******************************************************\r\n");
      printf("Target permit tile is set to %d\r\n", targetTile.tileColor.colorID);
      printf("******************************************************\r\n");
      printf("\r\n");
      setSPUDTargetTile(targetTile);
    }
    
    else {
      // Pick up best miner
      uint16_t minDist = 65535;
      minerIdx = 0;
      // loop through all miners
      for (int i = 0; i <2; i++) {
        // check if current miner is not in one of our permits
        bool inOurPermit = false;
        uint8_t currMinerTileID = OurTeamObject.minerArray[i].currentLoc.tileColor.colorID;
        printf("Miner idx %d is in tile %d\r\n", i , currMinerTileID);
        for (int j = 0; j < 3; j++) {
          // it is so skip this one
          if (OurTeamObject.permitLocs[j].tileColor.colorID == currMinerTileID) {
            inOurPermit = true;
            break;
          }
        }
        // not in our locations so move it
        if (!inOurPermit) {
          uint16_t dist = computeTileDistance(currentLoc, OurTeamObject.minerArray[i].currentLoc);
          printf("Distance computed is: %d\r\n", dist);
          if (dist < minDist) {
            minDist = dist;
            minerIdx = i;
          } 
        }
      }
      targetMiner = OurTeamObject.minerArray[minerIdx];
      printf("Target miner is %d.\r\n", minerIdx);
//				setSPUDTargetMINER(targetMiner);
      setSPUDTargetMINER_Index(minerIdx);
    }
    
    ES_Event_t Event2Post;
    Event2Post.EventType = ES_GOAL_SET;
    PostMasterSM(Event2Post);
        
}