/****************************************************************************
 Module
   EncoderService.c

 Revision
   1.0.1

 Description
   This is a Encoder file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from EncoderFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "bitdefs.h"


// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "EncoderService.h"
//#include "PWMService.h"
#include "ADService.h"
#include "PWMLibrary.h"
#include "Hardware.h"
#include "DriveSM.h"
#include "MasterSM.h"
/*----------------------------- Module Defines ----------------------------*/
// 40,000 ticks per mS assumes a 40Mhz clock
#define TicksPerMS 40000
#define PulsePerRev 3
#define GearRatio 50
#define mmPerClick 0.477
#define COLLISION_TIME 200
#define COLLISION_TIME_SEC COLLISION_TIME/1000
#define TicksPerRev 150
#define TicksThreshold 5
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void InitPeriodicInt( void );
void LeftControlLaw(void);
void RightControlLaw(void);
float clamp(float val, float clampL, float clampH);
void setTargetLeftMotorRPM(uint8_t speed);
void setTargetLeftMotorEncoderCount(float distanceMM);
static uint32_t CalcLeftRPM(void);
void PeriodicIntResponse(void);
void InitLeftEncoder(void);
void InitRightEncoder(void);
static uint32_t CalcRightRPM(void);
void setLeftForward(bool isForward);
void setRightForward(bool isForward);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint32_t LeftPeriod, RightPeriod;
static uint32_t LeftLastCapture, RightLastCapture;
static float iGainLeft, pGainLeft, dGainLeft, iGainRight, pGainRight, dGainRight, TargetRPM;
static float RequestedLeftDuty;
static float RequestedRightDuty;


// Left Motor
static float TargetLeftMotorRPM;
static uint32_t TargetLeftMotorEncoderCount;
static uint32_t LeftBrakingCount;
static uint32_t CurrentLeftMotorEncoderCount;
static uint32_t PrevLeftEncoderCount;
static bool leftForward;

// Right Motor
static float TargetRightMotorRPM;
static uint32_t TargetRightMotorEncoderCount;
static uint32_t RightBrakingCount;
static uint32_t CurrentRightMotorEncoderCount;
static uint32_t PrevRightEncoderCount;
static bool rightForward;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitEncoderService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitEncoderService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  InitPeriodicInt();
  InitLeftEncoder();
  InitRightEncoder();
  
  // PID Gains
  iGainLeft = 0.01;
  pGainLeft = 0.8;
  dGainLeft = 0.0;
  iGainRight = 0.01;
  pGainRight = 0.8;
  dGainRight = 0.0;
  
	ES_Timer_InitTimer(SpeedTimer, 200);
  ES_Timer_InitTimer(CollisionCheckTimer, COLLISION_TIME);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}
/****************************************************************************
 Function
     PostEncoderService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostEncoderService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}



/****************************************************************************
 Function
    RunEncoderService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunEncoderService(ES_Event_t ThisEvent)
{
  uint32_t PotVal;
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
	if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SpeedTimer){
    
//      printf("\r\n");
//    printf("Right Target RPM is %u\r\n", (uint32_t) TargetRightMotorRPM);
//		printf("Right Actual speed is %u\r\n", CalcRightRPM());
////    printf("Right Actual period is %u\r\n", RightPeriod);
//		printf("Target Right Encoder count is %d\r\n", TargetRightMotorEncoderCount);
////    printf("Right Duty cycle is %f\r\n", RequestedRightDuty);
//    printf("Right Encoder count is %d\r\n", CurrentRightMotorEncoderCount);
//    
//    printf("Left Target RPM is %u\r\n", (uint32_t) TargetLeftMotorRPM);
//		printf("Left Actual RPM is %u\r\n", CalcLeftRPM());
//////    printf("Left Actual period is %u\r\n", LeftPeriod);
////    printf("Left Duty cycle is %f\r\n", RequestedLeftDuty);
//		printf("Target Left Encoder count is %d\r\n", TargetLeftMotorEncoderCount);
//    printf("Left Encoder count is %d\r\n", CurrentLeftMotorEncoderCount);
//    printf("\r\n");
		ES_Timer_InitTimer(SpeedTimer, 1000);
	}
  
  if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == CollisionCheckTimer){
//    printf("Expected encoder ticks %f\r\n", TicksPerRev*TargetLeftMotorRPM*COLLISION_TIME_SEC/60);
    if(TargetLeftMotorRPM != 0){
//      if((CurrentLeftMotorEncoderCount - PrevLeftEncoderCount) < (TicksPerRev*TargetLeftMotorRPM*COLLISION_TIME_SEC/60- TicksThreshold)){
        // Not moving as fast as we expect
				 if(CurrentLeftMotorEncoderCount == PrevLeftEncoderCount){
       
        printf("STALL DETECTED BY LEFT ENCODER \r\n");
        ES_Event_t E2P;
        E2P.EventType = COLLISION_EVENT;
        PostMasterSM(E2P);
      }
      PrevLeftEncoderCount = CurrentLeftMotorEncoderCount;
    }
    
    if(TargetRightMotorRPM != 0){
//      if((CurrentRightMotorEncoderCount - PrevRightEncoderCount) < (TicksPerRev*TargetRightMotorRPM*COLLISION_TIME_SEC/60- TicksThreshold)){
        // Not moving as fast as we expect
			if(CurrentRightMotorEncoderCount == PrevRightEncoderCount){
        printf("STALL DETECTED BY RIGHT ENCODER \r\n");
        ES_Event_t E2P;
        E2P.EventType = COLLISION_EVENT;
        PostMasterSM(E2P);
      }
      PrevRightEncoderCount = CurrentRightMotorEncoderCount;
    }
    ES_Timer_InitTimer(CollisionCheckTimer, COLLISION_TIME);
  }
  
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void InitPeriodicInt( void ){
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0; // kill a few cycles to let the clock get going
  while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0) {
  }
  // make sure that timer (Timer B) is disabled before configuring
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // set up timer B in periodic mode so that it repeats the time-outs
  HWREG(WTIMER0_BASE+TIMER_O_TBMR) = (HWREG(WTIMER0_BASE+TIMER_O_TBMR)& ~TIMER_TBMR_TBMR_M)| TIMER_TBMR_TBMR_PERIOD;
  // set timeout to 100mS
  HWREG(WTIMER0_BASE+TIMER_O_TBILR) = TicksPerMS * 2;
  // enable a local timeout interrupt
  HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
  // enable the Timer B in Wide Timer 0 interrupt in the NVIC // it is interrupt number 95 so appears in EN2 at bit 30
  HWREG(NVIC_EN2) |= BIT31HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to // stall while stopped by the debugger 
  HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL); 
  
  // Priority (Controller needs to be lower than both encoder input captures)
  HWREG(NVIC_PRI23) = (HWREG(NVIC_PRI23) & ~NVIC_PRI23_INTD_M) + (2 << NVIC_PRI23_INTD_S);
}

void PeriodicIntResponse( void ){
// start by clearing the source of the interrupt
 HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
// increment our counter so that we can tell this is running
 LeftControlLaw();
 RightControlLaw();
}

/****************************************************************************
 Function
     InitLeftEncoder

 Parameters
     void

 Returns
     void

 Description
     Initialize Left Encoder to Wide Timer 0 Sub Timer A
 Notes

 Author
     PVK, 10/23/11, 19:25
****************************************************************************/
void InitLeftEncoder(void){
  // PC4
// start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER0_BASE+TIMER_O_TAPR) = 0;
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE+TIMER_O_TAMR) = (HWREG(WTIMER0_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT4HI;
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT0CCP0, 16 to shift it over to the
  // right nibble for bit 4 (4 bits/nibble * 4 bits)
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + (7<<16);
  // Enable pin on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT4HI;

  // make pin 4 on Port C into an input
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT4LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94 so apppears in EN2 at bit 30
  HWREG(NVIC_EN2) |= BIT30HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  printf("Left encoder init \r\n");
}

/****************************************************************************
 Function
     InitRightEncoder

 Parameters
     void

 Returns
     void

 Description
     Initialize Right Encoder to Wide Timer 1 Sub Timer A
 Notes

 Author
     PVK, 10/23/11, 19:25
****************************************************************************/
void InitRightEncoder(void){
  // PC6
// start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER1_BASE+TIMER_O_TAILR) = 0xffffffff;
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER1_BASE+TIMER_O_TAPR) = 0;
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER1_BASE+TIMER_O_TAMR) = (HWREG(WTIMER1_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) | (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 6 (WT1CCP0)
  HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT6HI;
  // Then, map bit 6's alternate function to WT1CCP0
  // 7 is the mux value to select WT0CCP0, 24 to shift it over to the
  // right nibble for bit 6 (4 bits/nibble * 6 bits)
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xf0ffffff) + (7<<24);
  // Enable pin 6 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT6HI;

  // make pin 6 on Port C into an input
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT6LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 96 so apppears in EN3 at bit 0
  HWREG(NVIC_EN3) |= BIT0HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}


/****************************************************************************
 Function
     LeftEncoderInterruptService

 Parameters
     void

 Returns
     void

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
void LeftEncoderInterruptService(void){
  uint32_t ThisCapture;
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER0_BASE+TIMER_O_TAR);
  LeftPeriod = ThisCapture - LeftLastCapture;
  // update LastCapture to prepare for the next edge
  LeftLastCapture = ThisCapture;  
  CurrentLeftMotorEncoderCount++;
  // when to send
  if (CurrentLeftMotorEncoderCount >= TargetLeftMotorEncoderCount && TargetLeftMotorEncoderCount > 0) {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_STOP;
    PostDriveSM(NewEvent);
  }
}

void RightEncoderInterruptService(void){
  uint32_t ThisCapture;
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER1_BASE+TIMER_O_TAR);
  RightPeriod = ThisCapture - RightLastCapture;
  // update LastCapture to prepare for the next edge
  RightLastCapture = ThisCapture;
  
  
  CurrentRightMotorEncoderCount++;
  
  // when to send
  if (CurrentRightMotorEncoderCount >= TargetRightMotorEncoderCount && TargetRightMotorEncoderCount > 0) {
    ES_Event_t NewEvent;
    NewEvent.EventType = ES_STOP;
    PostDriveSM(NewEvent);
  }
}


static uint32_t CalcLeftRPM(void){
 return (uint32_t) (60000*TicksPerMS/(PulsePerRev*LeftPeriod))/GearRatio;
}


static uint32_t CalcRightRPM(void){
 return (uint32_t) (60000 * TicksPerMS) / (PulsePerRev * RightPeriod * GearRatio);
}


void LeftControlLaw(void) {
  static float LeftIntegralTerm=0.0; /* integrator control effort */
  static float LeftDerivativeTerm; /* derivative control effort */
  static float LeftRPMError; /* make static for speed */
  static float LeftLastError=0.0; /* for Derivative Control */
  float CurrLeftRPM;
  //static uint32_t ThisPeriod; /* make static for speed */
  
  CurrLeftRPM = CalcLeftRPM();
  LeftRPMError = TargetLeftMotorRPM - CurrLeftRPM;
  LeftIntegralTerm += iGainLeft * LeftRPMError;
  LeftIntegralTerm = clamp(LeftIntegralTerm, 0, 100/pGainLeft); /* anti-windup */
  LeftDerivativeTerm = dGainLeft * (LeftRPMError - LeftLastError);
  RequestedLeftDuty = (pGainLeft*((LeftRPMError)+ LeftIntegralTerm + LeftDerivativeTerm));
  RequestedLeftDuty = clamp(RequestedLeftDuty, 0, 100);
  LeftLastError = LeftRPMError; // update last error
	
	if (TargetLeftMotorRPM == 0) {
		RequestedLeftDuty = 0;
		LeftLastError = 0;
		LeftRPMError = 0;
	}
	
  if (leftForward) {
    SetDuty(RequestedLeftDuty, 0); // channel 0 for left motor
  }
  else {
    RequestedLeftDuty = 100 - RequestedLeftDuty;
    SetDuty(RequestedLeftDuty, 0); // channel 0 for left motor
  }
}


void RightControlLaw(void) {
  static float RightIntegralTerm=0.0; /* integrator control effort */
  static float RightDerivativeTerm; /* derivative control effort */
  static float RightRPMError; /* make static for speed */
  static float RightLastError=0.0; /* for Derivative Control */
  float CurrRightRPM;
  //static uint32_t ThisPeriod; /* make static for speed */
  
  CurrRightRPM = CalcRightRPM();
  RightRPMError = TargetRightMotorRPM - CurrRightRPM;
  RightIntegralTerm += iGainRight * RightRPMError;
  RightIntegralTerm = clamp(RightIntegralTerm, 0, 100/pGainRight); /* anti-windup */
  RightDerivativeTerm = dGainRight * (RightRPMError - RightLastError);
  RequestedRightDuty = (pGainRight*((RightRPMError)+ RightIntegralTerm + RightDerivativeTerm));
  RequestedRightDuty = clamp(RequestedRightDuty, 0, 100);
  RightLastError = RightRPMError; // update last error
	
	if (TargetRightMotorRPM == 0) {
		RequestedRightDuty = 0;
		RightLastError = 0;
		RightRPMError = 0;
	}
  if (rightForward) {
  SetDuty(RequestedRightDuty, 1); // channel 1 for Right motor
  }
  else {
    RequestedRightDuty = 100 - RequestedRightDuty;
    SetDuty(RequestedRightDuty, 1); // channel 1 for Right motor
  }
}


// constrain val to be in range clampL to clampH
float clamp(float val, float clampL, float clampH) {
  if (val > clampH){ // if too high
    return clampH;
  }
  if (val < clampL) {// if too low
    return clampL;
  }
  return val; // if OK as-is
}


void setTargetLeftMotorRPM(uint8_t speed) {
  TargetLeftMotorRPM = speed;
}

void setTargetLeftMotorEncoderCount(float distanceMM) {
  ES_Timer_InitTimer(CollisionCheckTimer, 1000);
  CurrentLeftMotorEncoderCount = 0; // reset odometer
  PrevLeftEncoderCount = 0 ;
  TargetLeftMotorEncoderCount = distanceMM * mmPerClick; // should automatically convert to uint32_t
}

void setTargetRightMotorRPM(uint8_t speed) {
  TargetRightMotorRPM = speed;
}

void setTargetRightMotorEncoderCount(float distanceMM) {
  CurrentRightMotorEncoderCount = 0; // reset odometer
  PrevRightEncoderCount = 0 ;
  TargetRightMotorEncoderCount = distanceMM * mmPerClick; // should automatically convert to uint32_t
}

void setLeftForward(bool isForward) {
  leftForward = isForward;
}

void setRightForward(bool isForward) {
  rightForward = isForward;
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

