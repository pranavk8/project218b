/****************************************************************************
 Module
   NavigatingSM.c

 Revision
   2.0.1

 Description
   This is a template file for implementing state machines.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/27/17 09:48 jec      another correction to re-assign both CurrentEvent
                         and ReturnEvent to the result of the During function
                         this eliminates the need for the prior fix and allows
                         the during function to-remap an event that will be
                         processed at a higher level.
 02/20/17 10:14 jec      correction to Run function to correctly assign 
                         ReturnEvent in the situation where a lower level
                         machine consumed an event.
 02/03/16 12:38 jec      updated comments to reflect changes made in '14 & '15
                         converted unsigned char to bool where appropriate
                         spelling changes on true (was True) to match standard
                         removed local var used for debugger visibility in 'C32
                         commented out references to Start & RunLowerLevelSM so
                         that this can compile. 
 02/07/13 21:00 jec      corrections to return variable (should have been
                         ReturnEvent, not CurrentEvent) and several EV_xxx
                         event names that were left over from the old version
 02/08/12 09:56 jec      revisions for the Events and Services Framework Gen2
 02/13/10 14:29 jec      revised Start and run to add new kind of entry function
                         to make implementing history entry cleaner
 02/13/10 12:29 jec      added NewEvent local variable to During function and
                         comments about using either it or Event as the return
 02/11/10 15:54 jec      more revised comments, removing last comment in during
                         function that belongs in the run function
 02/09/10 17:21 jec      updated comments about internal transitions on During funtion
 02/18/09 10:14 jec      removed redundant call to RunLowerlevelSM in EV_Entry
                         processing in During function
 02/20/07 21:37 jec      converted to use enumerated type for events & states
 02/13/05 19:38 jec      added support for self-transitions, reworked
                         to eliminate repeated transition code
 02/11/05 16:54 jec      converted to implment hierarchy explicitly
 02/25/03 10:32 jec      converted to take a passed event parameter
 02/18/99 10:19 jec      built template from MasterMachine.c
 02/14/99 10:34 jec      Began Coding
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "MiningOpsInfo.h"
#include "ColorSensor.h"
#include "AccelSPIFSM.h"
#include "SensorsService.h"
#include "DriveSM.h"
#include "NavigatingSM.h"
#include "MasterSM.h"
#include "NormalOpsSM.h"
#include "SpudSPI.h"
/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE STATE_ZERO

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/
static ES_Event_t DuringNavRest( ES_Event_t Event);
static ES_Event_t DuringAlign2Goal( ES_Event_t Event);
static ES_Event_t DuringDriving2Goal( ES_Event_t Event);
static ES_Event_t DuringNavExploring( ES_Event_t Event);
static ES_Event_t DuringReversing( ES_Event_t Event);
int16_t amountToRotate(GridTile_t currentTile, GridTile_t targetTile);
void GenerateAndPostRotateEvent(GridTile_t targetTile); 
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static NavigatingState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunNavigatingSM

 Parameters
   ES_Event_t: the event to process

 Returns
   ES_Event_t: an event to return

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 2/11/05, 10:45AM
****************************************************************************/
ES_Event_t RunNavigatingSM( ES_Event_t CurrentEvent )
{
   bool MakeTransition = false;/* are we making a state transition? */
   NavigatingState_t NextState = CurrentState;
   ES_Event_t EntryEventKind = { ES_ENTRY, 0 };// default to normal entry to new state
   ES_Event_t ReturnEvent = CurrentEvent; // assume we are not consuming event
   ES_Event_t Event2Post;
   
   switch ( CurrentState )
   {
       case NavRest :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringNavRest(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
//					if(getStatus() != 3){
            switch (CurrentEvent.EventType)
            {
               case ES_TIMEOUT : //If event is event one
                 // Uncomment the following after functionality is added.
                 if (CurrentEvent.EventParam == PauseTimer) {
                   if (!GetIfMinerIsPickedUp()){
                     setIRChecker(true);
                     setTargetFrequency(getTargetMiner().freq);
                     printf("Looking for miner freq = %d\r\n", getTargetMiner().freq);
                     Event2Post.EventType = ES_ROTATE_CW;
                     Event2Post.EventParam = 359;
                     PostDriveSM(Event2Post);
                   }
                   
                   else{
                     setIRChecker(false);
                     GridTile_t targetTile = getTargetTile();
										 setCheckMinerFlag(true);
                     GenerateAndPostRotateEvent(targetTile);
                     printf("Aligning with target tile \r\n");
                   }
                 }
                  // Execute action function for state one : event one
                  NextState = Align2Goal;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                // repeat cases as required for relevant events
            }
         }
//			 }
       break;
         
       case Align2Goal :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringAlign2Goal(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case ES_IR_DETECTED : //If event is event one
                  // Execute action function for state one : event one
                  printf("IR found \r\n");
                  Event2Post.EventType = ES_MASTER_STOP;
                  PostDriveSM(Event2Post);
                  Event2Post.EventType = ES_FORWARD;
                  Event2Post.EventParam = 300;    //Need to tune that
                  PostDriveSM(Event2Post);
                  NextState = Driving2Goal;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
               
               case ES_MOTION_COMPLETE : //If event is event one
                  // Execute action function for state one : event one
                  if(!GetIfMinerIsPickedUp()){
                    // Failed to find IR so spiral
                    printf("Can't find miner so spiral \r\n");
                    Event2Post.EventType = ES_SPIRAL_FORWARD;
                    Event2Post.EventParam = 300;
                    PostDriveSM(Event2Post);
                  }
                  else{
                    // Aligned with target square
                    Event2Post.EventType = ES_FORWARD;
                    Color_t presentCol = getCurrentColor();
                    GridTile_t presentTile = getCurrentTile(presentCol);
                    if (computeTileDistance(presentTile, getTargetTile()) == 0) {
                      Event2Post.EventType = ES_ROTATE_CW;
                      Event2Post.EventParam = 180;
                      PostDriveSM(Event2Post);
                    }
                    else {
                      Event2Post.EventParam = 0.6*computeTileDistance(presentTile, getTargetTile());
                      printf("Going to target tile \r\n");
                      PostDriveSM(Event2Post);
                    }
                    //Need to tune that
                    
                  }
                  
                  NextState = Driving2Goal;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
									
                case MINER_REACHED_DESTINATION : //If event is event one
                  // Execute action function for state one : event one
               // Miner reaches target square posted by SpudSPI so going reverse to drop miner
                  Event2Post.EventType = ES_MASTER_STOP;
                  PostDriveSM(Event2Post);
                  setEM(false);
                  // Set beam break checker OFF
                  setBeamBreak(false);
                  Event2Post.EventType = ES_REVERSE;
                  Event2Post.EventParam = 300;    // Need to tune this
                  PostDriveSM(Event2Post);
									printf("Dropped MINER, reversing\r\n");
                  
                  NextState = Reversing;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
									
                // repeat cases as required for relevant events
            }
         }
       break;  
         
       case Driving2Goal :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringDriving2Goal(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case ES_BEAM_BROKEN : //If event is event one
                  // Execute action function for state one : event one
                  printf("Picked up miner \r\n");
                  SetMinerPickedUp(true);
                  setIRChecker(false);
                  setEM(true);
                  printf("Turning ON EM");
                  setCheckMinerFlag(true);
                  Event2Post.EventType = ES_MASTER_STOP;
                  PostDriveSM(Event2Post);
									Event2Post.EventType = ES_FORWARD;
									Event2Post.EventParam = 100;
									PostDriveSM(Event2Post);
                  Event2Post.EventType = ES_TARGET_REACHED;
                  PostMasterSM(Event2Post);
                  
                  NextState = NavRest;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
               
               case ES_MOTION_COMPLETE : //If event is event one
                  // Execute action function for state one : event one
                  if(!GetIfMinerIsPickedUp()){
                    // Not yet reached IR so realigning
                    ES_Timer_InitTimer(PauseTimer, 100);
                    NextState = NavRest;
                  }
                  else{
                    // Not reached target square yet, so spiral
                    ES_Timer_InitTimer(PauseTimer, 2000);
                    NextState = NavRest;
                  }
                  
                  //Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                  
                  
               case MINER_REACHED_DESTINATION : //If event is event one
                  // Execute action function for state one : event one
               // Miner reaches target square posted by SpudSPI so going reverse to drop miner
                  Event2Post.EventType = ES_MASTER_STOP;
                  PostDriveSM(Event2Post);
                  setEM(false);
                  // Set beam break checker OFF
                  setBeamBreak(false);
                  Event2Post.EventType = ES_REVERSE;
                  Event2Post.EventParam = 300;    // Need to tune this
                  PostDriveSM(Event2Post);
									printf("Dropped MINER, reversing\r\n");
                  
                  NextState = Reversing;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                // repeat cases as required for relevant events
            }
         }
       break;  
         
         
       case NavExploring :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringNavExploring(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {
               case MINER_REACHED_DESTINATION : //If event is event one
                  // Execute action function for state one : event one
               // Miner reaches target square posted by SpudSPI so going reverse to drop miner
                  Event2Post.EventType = ES_MASTER_STOP;
                  PostDriveSM(Event2Post);
                  setEM(false);
                  // Set beam break checker OFF
                  setBeamBreak(false);
                  Event2Post.EventType = ES_REVERSE;
									printf("Reversing in Nav_explore \r\n");
                  Event2Post.EventParam = 300;    // Need to tune this
                  PostDriveSM(Event2Post);
                  
                  NextState = Reversing;//Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                  MakeTransition = true; //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
               
               case ES_MOTION_COMPLETE : //If event is event one
                  // Execute action function for state one : event one
                  if(!GetIfMinerIsPickedUp()){
                    // Done exploring, now try to realign
                    Event2Post.EventType = ES_ROTATE_CW;
                    Event2Post.EventParam = 359;
                    PostDriveSM(Event2Post);
                    NextState = Align2Goal;
                    MakeTransition = true;
                  }
                  
                  //Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                   //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                // repeat cases as required for relevant events
            }
         }
       break;  
         
       case Reversing :       // If current state is state one
         // Execute During function for state one. ES_ENTRY & ES_EXIT are
         // processed here allow the lower level state machines to re-map
         // or consume the event
         ReturnEvent = CurrentEvent = DuringReversing(CurrentEvent);
         //process any events
         if ( CurrentEvent.EventType != ES_NO_EVENT ) //If an event is active
         {
            switch (CurrentEvent.EventType)
            {  
               case ES_MOTION_COMPLETE : //If event is event one
                  // Execute action function for state one : event one
//                // Droppped miner so post TARGET_REACHED to master
                    Event2Post.EventType = ES_TARGET_REACHED;
										printf("Posting TARGET_REACHED to miner \r\n");
										SetMinerPickedUp(false);
                    PostMasterSM(Event2Post);
                    NextState = NavRest;
                    MakeTransition = true;
//                  }
                  
                  //Decide what the next state will be
                  // for internal transitions, skip changing MakeTransition
                   //mark that we are taking a transition
                  // if transitioning to a state with history change kind of entry
//                  EntryEventKind.EventType = ES_ENTRY_HISTORY;
                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
                  break;
                // repeat cases as required for relevant events
            }
         }
       break;  
      // repeat state pattern as required for other states
    }
    //   If we are making a state transition
    if (MakeTransition == true)
    {
       //   Execute exit function for current state
       CurrentEvent.EventType = ES_EXIT;
       RunNavigatingSM(CurrentEvent);

       CurrentState = NextState; //Modify state variable

       //   Execute entry function for new state
       // this defaults to ES_ENTRY
       RunNavigatingSM(EntryEventKind);
     }
     return(ReturnEvent);
}
/****************************************************************************
 Function
     StartNavigatingSM

 Parameters
     None

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 2/18/99, 10:38AM
****************************************************************************/
void StartNavigatingSM ( ES_Event_t CurrentEvent )
{
   // to implement entry to a history state or directly to a substate
   // you can modify the initialization of the CurrentState variable
   // otherwise just start in the entry state every time the state machine
   // is started
//   if ( ES_ENTRY_HISTORY != CurrentEvent.EventType )
//   {
        CurrentState = NavRest;
//   }
   // call the entry function (if any) for the ENTRY_STATE
   RunNavigatingSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryNavigatingSM

 Parameters
     None

 Returns
     NavigatingState_t The current state of the Navigating state machine

 Description
     returns the current state of the Navigating state machine
 Notes

 Author
     J. Edward Carryer, 2/11/05, 10:38AM
****************************************************************************/
NavigatingState_t QueryNavigatingSM ( void )
{
   return(CurrentState);
}

/***************************************************************************
 private functions
 ***************************************************************************/

static ES_Event_t DuringNavRest( ES_Event_t Event)
{
    ES_Event_t Event2Post;
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY )
    {
        // implement any entry actions required for this state machine
        Event2Post.EventType = ES_MASTER_STOP;
        PostDriveSM(Event2Post);
        printf("Entered NavigationSM in rest\r\n");
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
//			Event2Post.EventType = ES_MASTER_STOP;
//        PostDriveSM(Event2Post);
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

static ES_Event_t DuringAlign2Goal ( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY )
    {
        // implement any entry actions required for this state machine
        
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

static ES_Event_t DuringDriving2Goal ( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY )
    {
        // implement any entry actions required for this state machine
        
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

static ES_Event_t DuringNavExploring ( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY )
    {
        // implement any entry actions required for this state machine
        
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

static ES_Event_t DuringReversing ( ES_Event_t Event)
{
    ES_Event_t ReturnEvent = Event; // assume no re-mapping or consumption

    // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    if ( Event.EventType == ES_ENTRY )
    {
        // implement any entry actions required for this state machine
        
        // after that start any lower level machines that run in this state
        //StartLowerLevelSM( Event );
        // repeat the StartxxxSM() functions for concurrent state machines
        // on the lower level
    }
    else if ( Event.EventType == ES_EXIT )
    {
        // on exit, give the lower levels a chance to clean up first
        //RunLowerLevelSM(Event);
        // repeat for any concurrently running state machines
        // now do any local exit functionality
      
    }else
    // do the 'during' function for this state
    {
        // run any lower level state machine
        // ReturnEvent = RunLowerLevelSM(Event);
      
        // repeat for any concurrent lower level machines
      
        // do any activity that is repeated as long as we are in this state
    }
    // return either Event, if you don't want to allow the lower level machine
    // to remap the current event, or ReturnEvent if you do want to allow it.
    return(ReturnEvent);
}

int16_t amountToRotate(GridTile_t currentTile, GridTile_t targetTile) {
  return computeAngularDifference(currentTile, targetTile) - GetCurrentHeading();
}

void GenerateAndPostRotateEvent(GridTile_t targetTile) {
  ES_Event_t NewEvent;
  
  // read current tile color
//  Color_t currentColor = getCurrentColor();
  // use color to get the current tile
//  GridTile_t currentTile = getCurrentTile(currentColor);
//  printf("Current tile: %d\r\n", currentTile.tileColor.colorID);
  GridTile_t currentTile = getCurrLoc();
	
	
  // get the target tile (this should be set in the planning state)
  int16_t TargetAngle, TargetDist;
  
  // compute how much you want to rotate based on current heading and target heading
  TargetAngle = amountToRotate(currentTile, targetTile);
	printf("Target angle %d\r\n", TargetAngle);
  TargetDist = computeTileDistance(currentTile, targetTile);
	
	if (currentTile.tileColor.colorID == targetTile.tileColor.colorID) {
		// already in same tile
		TargetAngle = 0;
	}
  
  // rotate in the right direction
  if (TargetAngle > 0) {
    if(TargetAngle > 180){
      NewEvent.EventType = ES_ROTATE_CW;
      NewEvent.EventParam = 360 - TargetAngle;
      printf("CLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
    else{
      NewEvent.EventType = ES_ROTATE_CCW;
      NewEvent.EventParam = TargetAngle;
      printf("COUNTERCLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
  }
  else if (TargetAngle < 0) {
    if (TargetAngle > -180){
      NewEvent.EventType = ES_ROTATE_CW;
      NewEvent.EventParam = abs(TargetAngle);
      printf("CLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
    else{
      NewEvent.EventType = ES_ROTATE_CCW;
      NewEvent.EventParam = 360 - abs(TargetAngle);
      printf("COUNTERCLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
  }
  else {
    NewEvent.EventType = ES_ROTATE_CCW;
    NewEvent.EventParam = 0;
  }
    PostDriveSM(NewEvent);
    printf("Aligning with goal\r\n");
}
