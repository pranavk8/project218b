/****************************************************************************
 Module
   ColorSensor.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:26 jec     moves definition of ALL_BITS to ES_Port.h
 10/19/17 21:28 jec     meaningless change to test updating
 10/19/17 18:42 jec     removed referennces to driverlib and programmed the
                        ports directly
 08/21/17 21:44 jec     modified LED blink routine to only modify bit 3 so that
                        I can test the new new framework debugging lines on PF1-2
 08/16/17 14:13 jec      corrected ONE_SEC constant to match Tiva tick rate
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/

// Hardware
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

// Our application
#include "I2CService.h"
#include "ColorSensor.h"
#include "MiningOpsInfo.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC / 2)
#define TWO_SEC (ONE_SEC * 2)
#define FIVE_SEC (ONE_SEC * 5)

#define CLEAR_THRESHOLD 2500


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
int8_t GetColor(float ClearVal, float R, float G, float B);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

// Need to add all 16 colors and tune values
// Mean value definitions for colors
static float WhiteColor[] = {35.0, 33.37, 28.6};
static float RedColor[] = {58.9, 21.4, 21.8};
static float GreenColor[] = {31.8, 39.4, 26.9};
static float BlueColor[] = {21, 33.1, 44.7};
static float YellowColor[] = {46.65, 33.5, 17.1};
static float PurpleColor[] = {42.2, 26.48, 31.9};
static float MagentaColor[] = {50.7, 23.4, 27.4};
static float CyanColor[] = {25.47, 36.2, 36.48};
static float OrangeColor[] = {54.2, 26.31, 19.7};
static float BlackColor[] = {38.6, 31.7, 28.5};
static float OliveColor[] = {44.4, 33.5, 20.61};
static float NavyColor[] = {34.7, 30.23, 34.5};
static float ApricotColor[] = {41.6, 31.05, 25.1};
static float BrownColor[] = {49.4, 29.15, 21.5};
static float MaroonColor[] = {49.9, 26.0, 24.2};
static float BeigeColor[] = {38.35, 34.0, 24.7};
static Color_t prevColor;
// Threshold definitions for colors
static float Threshold[] = {2, 2, 2};
static Color_t measuredColor, mappedColor;

// Test Table
#define column 5
#define row 3
int i = 0;
int j = 0;

int table[row][column] =
{
    {0, 1, 2, 3, 4},
    {10, 11, 12, 13, 14},
    {20, 21, 22, 23, 24}
};



/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitColorSensor

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 02/03/19, 10:46
****************************************************************************/
bool InitColorSensor(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostColorSensor

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
J. Edward Carryer, 02/03/19, 10:48
****************************************************************************/
bool PostColorSensor(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunColorSensor

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
J. Edward Carryer, 02/03/19, 10:49
****************************************************************************/
ES_Event_t RunColorSensor(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      ES_Timer_InitTimer(I2C_TEST_TIMER, 200);
      puts("I2C TestHarness:");
      printf("\rES_INIT received in Service %d\r\n", MyPriority);
    }
    break;
    case ES_TIMEOUT:   // re-start timer & announce
    {
      uint16_t ClearValue;
      uint16_t RedValue;
      uint16_t GreenValue;
      uint16_t BlueValue;
      uint8_t ColorEstimate;
      float RedPercent;
      float GreenPercent;
      float BluePercent;
      
      
      ClearValue = I2C_GetClearValue();
      RedValue   = I2C_GetRedValue();
      GreenValue = I2C_GetGreenValue();
      BlueValue  = I2C_GetBlueValue();
      
      RedPercent =((float)RedValue*100/ClearValue);
      GreenPercent =((float)GreenValue*100/ClearValue);
      BluePercent = ((float)BlueValue*100/ClearValue);
      
      measuredColor.colorID = UNKNOWN;
      measuredColor.R = RedPercent;
      measuredColor.G = GreenPercent;
      measuredColor.B = BluePercent;
			
//			if (measuredColor.colorID >= 16) {
//				measuredColor = prevColor;
//			} else {
//				prevColor = measuredColor;
//			}
      
			mappedColor = computeColor(measuredColor);
//			printf("Current color: %d\r\n" , mappedColor.colorID);
      GridTile_t CurrentTile = getCurrentTile(measuredColor);
      
      if (ClearValue < CLEAR_THRESHOLD){
        printf("No color detected\r\n");
      }
      else{
//        GridTile_t CurrentTile = getCurrentTile(measuredColor);
//        printf("Current Tile: %d", CurrentTile.tileColor.colorID);
//        printf("\t x: %8.4f", CurrentTile.center_x);
//        printf("\t y: %8.4f\r\n", CurrentTile.center_y);
//        ColorEstimate = GetColor(ClearValue, RedPercent, GreenPercent, BluePercent);
//				switch (ColorEstimate)
//				{
//					case WHITE:
//						printf("White color detected \r\n");
//					break;
//					
//					case UNKNOWN_COL:
//						printf("Invalid color detected \r\n");
//					break;
//					
//					case NAVY:
//						printf("Navy color detected \r\n");
//					break;
//					
//					case LIME:
//						printf("Lime color detected \r\n");
//					break;
//					
//					case BROWN:
//						printf("Brown color detected \r\n");
//					break;
//					
//					case YELLOW:
//						printf("Yellow color detected \r\n");
//					break;
//					
//					case APRICOT:
//						printf("Apricot color detected \r\n");
//					break;
//					
//					case RED:
//						printf("Red color detected \r\n");
//					break;
//					
//					case PURPLE:
//						printf("Purple color detected \r\n");
//					break;
//					
//					case CYAN:
//						printf("Cyan color detected \r\n");
//					break;
//					
//					case OLIVE:
//						printf("Olive color detected \r\n");
//					break;
//					
//					case BEIGE:
//						printf("Beige color detected \r\n");
//					break;
//					
//					case ORANGE:
//						printf("Orange color detected \r\n");
//					break;
//					
//					case MAGENTA:
//						printf("Magenta color detected \r\n");
//					break;
//					
//					case LAVENDER:
//						printf("Lavender color detected \r\n");
//					break;
//					
//					case MAROON:
//						printf("Maroon color detected \r\n");
//					break;
//					
//					case BLUE:
//						printf("Blue color detected \r\n");
//					break;
//					
//					case MINT:
//						printf("Mint color detected \r\n");
//					break;
					
//				}
//        printf("Color detected as %u\r\n", ColorEstimate);
      }
//      printf("Clr: %d, Red: %d, Grn: %d, Blu: %d, R%%: %.2f, G%% %.2f, B%% %.2f \r\n",
//          ClearValue, RedValue, GreenValue, BlueValue, 
//          RedPercent, GreenPercent, BluePercent);

    }
		ES_Timer_InitTimer(I2C_TEST_TIMER, 200);
    break;
    
    default:
    {}
     break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

Color_t getCurrentColor(void) {
  return mappedColor;
}
//int8_t GetColor(float ClearVal, float R, float G, float B){
//  
//  /*Another idea for this is to create an array with all the colors and find
//  which has the least deviation from mean of a particular color (However, all colors
//  may not have the same variance in readings)
//  */
//  
//	if ((abs(R - WhiteColor[0]) <= Threshold[0]) && (abs(G - WhiteColor[1]) <= Threshold[1]) &&
//  (abs(B - WhiteColor[2]) <= Threshold[2])){
//    return WHITE;  
//  }
//	
//  //Checking if color is Navy
//  else if ((abs(R - NavyColor[0]) <= Threshold[0]) && (abs(G - NavyColor[1]) <= Threshold[1]) &&
//  (abs(B - NavyColor[2]) <= Threshold[2])){
//    return NAVY;  
//  }
////	Checking if color is Lime
////	else if ((abs(R - LimeColor[0]) <= Threshold[0]) && (abs(G - LimeColor[1]) <= Threshold[1]) &&
////  (abs(B - LimeColor[2]) <= Threshold[2])){
////    return LIME;  
////  
//	//Checking if color is Brown
//	else if ((abs(R - BrownColor[0]) <= Threshold[0]) && (abs(G - BrownColor[1]) <= Threshold[1]) &&
//  (abs(B - BrownColor[2]) <= Threshold[2])){
//    return BROWN;  
//  }
//	
//	//Checking if color is Yellow
//	else if ((abs(R - YellowColor[0]) <= Threshold[0]) && (abs(G - YellowColor[1]) <= Threshold[1]) &&
//  (abs(B - YellowColor[2]) <= Threshold[2])){
//    return YELLOW;  
//  }
//  
//	//Checking if color is Apricot
//	else if ((abs(R - ApricotColor[0]) <= Threshold[0]) && (abs(G - ApricotColor[1]) <= Threshold[1]) &&
//  (abs(B - ApricotColor[2]) <= Threshold[2])){
//    return APRICOT;  
//  }
//	
//	//Checking if color is Red
//  else if ((abs(R - RedColor[0]) <= Threshold[0]) && (abs(G - RedColor[1]) <= Threshold[1]) &&
//  (abs(B - RedColor[2]) <= Threshold[2])){
//    return RED;  
//  }
//	
//	//Checking if color is Purple
//  else if ((abs(R - PurpleColor[0]) <= Threshold[0]) && (abs(G - PurpleColor[1]) <= Threshold[1]) &&
//  (abs(B - PurpleColor[2]) <= Threshold[2])){
//    return PURPLE;  
//  }
//	
//	//Checking if color is Cyan
//  else if ((abs(R - CyanColor[0]) <= Threshold[0]) && (abs(G - CyanColor[1]) <= Threshold[1]) &&
//  (abs(B - CyanColor[2]) <= Threshold[2])){
//    return CYAN;  
//  }
//	
//	//Checking if color is Olive
//  else if ((abs(R - OliveColor[0]) <= Threshold[0]) && (abs(G - OliveColor[1]) <= Threshold[1]) &&
//  (abs(B - OliveColor[2]) <= Threshold[2])){
//    return OLIVE;  
//  }
//	
//	//Checking if color is Beige
//  else if ((abs(R - BeigeColor[0]) <= Threshold[0]) && (abs(G - BeigeColor[1]) <= Threshold[1]) &&
//  (abs(B - BeigeColor[2]) <= Threshold[2])){
//    return BEIGE;  
//  }
//	
//	//Checking if color is Orange
//  else if ((abs(R - OrangeColor[0]) <= Threshold[0]) && (abs(G - OrangeColor[1]) <= Threshold[1]) &&
//  (abs(B - OrangeColor[2]) <= Threshold[2])){
//    return ORANGE;  
//  }
//	
//	//Checking if color is Magenta
//  else if ((abs(R - MagentaColor[0]) <= Threshold[0]) && (abs(G - MagentaColor[1]) <= Threshold[1]) &&
//  (abs(B - MagentaColor[2]) <= Threshold[2])){
//    return MAGENTA;  
//  }
//	
////	//Checking if color is Lavender
////  else if ((abs(R - LavenderColor[0]) <= Threshold[0]) && (abs(G - LavenderColor[1]) <= Threshold[1]) &&
////  (abs(B - LavenderColor[2]) <= Threshold[2])){
////    return LAVENDER;  
////  }
//	
//	//Checking if color is Maroon
//  else if ((abs(R - MaroonColor[0]) <= Threshold[0]) && (abs(G - MaroonColor[1]) <= Threshold[1]) &&
//  (abs(B - MaroonColor[2]) <= Threshold[2])){
//    return MAROON;  
//  }
//	
//	//Checking if color is Blue
//  else if ((abs(R - BlueColor[0]) <= Threshold[0]) && (abs(G - BlueColor[1]) <= Threshold[1]) &&
//  (abs(B - BlueColor[2]) <= Threshold[2])){
//    return BLUE;  
//  }
//	
////	//Checking if color is Mint
////  else if ((abs(R - MintColor[0]) <= Threshold[0]) && (abs(G - MintColor[1]) <= Threshold[1]) &&
////  (abs(B - MintColor[2]) <= Threshold[2])){
////    return MINT;  
////  }
//  //if doesn't fit any color
//  else{
//    return UNKNOWN_COL;
//  }
//}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

