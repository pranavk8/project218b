/****************************************************************************
 Module
   IRService.c

 Revision
   1.0.1

 Description
   This is a IR file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from IRFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "bitdefs.h"


// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

//#include "IRService.h"
//#include "PWMService.h"
#include "ADService.h"
#include "PWMLibrary.h"
#include "Hardware.h"
/*----------------------------- Module Defines ----------------------------*/
// 40,000 ticks per mS assumes a 40Mhz clock
#define TicksPerMS 40000
#define PulsePerRev 3
#define GearRatio 50
#define mmPerClick 0.524;
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void InitIRInterrupt(void);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint32_t Period;
static bool isIRActive;
static uint32_t LastCapture;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitIRService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitIRService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  InitIRInterrupt();
 
  // Test Pin
//  activatePort(PORT_B);
//  setPinDigital(GPIO_PORTB_BASE, 5);
//  setPinDirection(GPIO_PORTB_BASE, 5, OUTPUT);
//  writeDigitalOutput(GPIO_PORTB_BASE, 5, LO);
  
//	ES_Timer_InitTimer(SpeedTimer, 200);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}
/****************************************************************************
 Function
     PostIRService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostIRService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}



/****************************************************************************
 Function
    RunIRService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunIRService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/


/****************************************************************************
 Function
     InitLeftIR

 Parameters
     void

 Returns
     void

 Description
     Initialize Left IR to Wide Timer 0 Sub Timer A
 Notes

 Author
     PVK, 10/23/11, 19:25
****************************************************************************/
void InitIRInterrupt(void) {
  // PC7
// start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER1_BASE+TIMER_O_TBILR) = 0xffffffff;
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER1_BASE+TIMER_O_TBPR) = 0;
  // set up timer A in capture mode (TBMR=3, TBAMS = 0),
  // for edge time (TBCMR = 1) and up-counting (TBCDIR = 1)
  HWREG(WTIMER1_BASE+TIMER_O_TBMR) = (HWREG(WTIMER1_BASE+TIMER_O_TBMR) & ~TIMER_TBMR_TBAMS) | (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
  // To set the event to rising edge, we need to modify the TBEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TBEVENT bits
  HWREG(WTIMER1_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEVENT_M;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 6 (WT1CCP0)
  HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT7HI;
  // Then, map bit 7's alternate function to WT1CCP1
  // 7 is the mux value to select WT1CCP1, 28 to shift it over to the
  // right nibble for bit 7 (4 bits/nibble * 7 bits)
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) = (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0x0fffffff) + (7<<28);
  // Enable pin 7 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT7HI;

  // make pin 7 on Port C into an input
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT7LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER1_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer B in Wide Timer 1 interrupt in the NVIC
  // it is interrupt number 97 so apppears in EN3 at bit 1
  HWREG(NVIC_EN3) |= BIT1HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}


/****************************************************************************
 Function
     LeftIRInterruptService

 Parameters
     void

 Returns
     void

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
void IRInterruptService(void){
  uint32_t ThisCapture;
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER1_BASE+TIMER_O_ICR) = TIMER_ICR_CBECINT;
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER1_BASE+TIMER_O_TBR);
  Period = ThisCapture - LastCapture;
  // update LastCapture to prepare for the next edge
  LastCapture = ThisCapture;
  writeDigitalOutput(GPIO_PORTB_BASE, 5, HI);
    
  // Check whether period is close to target frequency (bool fn that checks current vs target with some buffer)
  // if close && if ir isactive, only then post
}


void setIRChecker(bool on_off) {
  isIRActive = on_off;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

