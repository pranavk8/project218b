/****************************************************************************
 Module
   DCMotorService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "DCMotorService.h"
#include "EncoderService.h"
#include "PINDEFS.h"
#include "ADService.h"
#include "PWM16Tiva.h"
#include "PWMLibrary.h"

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <float.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/
#define PeriodInMicroS 166 // values were 5000, 4000, 2000, 1000, 500, 100 for 
                          // the frequencies tests (200, 250, 500, 1000, 2000, 10000)
#define PeriodInMS 5 // 1/200Hz*1000MS/S
#define PWMTicksPerMS 40000 / 32 // explanation below
// calculate the ticks per ms for the 40MHz clock speed
// 40e6 / 32 (prescaler) so this gives tick rate
// 1/(40e6 / 32) invert this to get the period per tick
// 1/(40e6 / 32) * 1000 convert period from seconds to ms
#define BitsPerNibble 4

// program generator A to go to 1 at rising comare A, 0 on falling compare A  
#define GenA_Normal (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO )
// program generator B to go to 1 at rising comare B, 0 on falling compare B  
#define GenB_Normal (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO )

// Motor directions
#define LEFT 0
#define RIGHT 1

// Motor speeds
#define LFULL 95
#define RFULL 100

#define LHALF 75
#define RHALF 80

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


bool CheckEvents(void);
uint32_t returnDutyCycle(void);
void setForward(void);
void setReverse(void);
void setCW(void);
void setCCW(void);
void actuateLMotor(uint8_t DC);
void actuateRMotor(uint8_t DC);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
uint32_t PWMPeriod = 170;
static bool forwardLeft = true;
static bool forwardRight = true;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDCMotorService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitDCMotorService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  
  /********************************************/
  // Initialize PWM Library
  InitPWMLibrary(2,PWMPeriod,0,0,0,0,0);
  
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI);
  // Set direction to input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= BIT2LO;
  //Set direction of both polarity pins: Forward
  
  setForward();
  actuateLMotor(0);
  actuateRMotor(0);
  
  /*******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostDCMotorService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDCMotorService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDCMotorService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDCMotorService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************/
  switch(ThisEvent.EventType)
  {
    case ES_STOP: //Stop event
    {
      setForward();
      actuateLMotor(0);
      actuateRMotor(0);
    }
    break;
    
    case ES_FORWARD_FULL: //Full speed event
    {
      setForward();
      actuateLMotor(LFULL);
      actuateRMotor(RFULL);
    }
    break;
    
    case ES_FORWARD_HALF: //Half Speed event
    {
      setForward();
      actuateLMotor(LHALF);
      actuateRMotor(RHALF);
    }
    break;
    
    case ES_BACKWARD_FULL: //Backwards full Speed event
    {
      setReverse();
      actuateLMotor(LFULL);
      actuateRMotor(RFULL);
    }
    break;
    
    case ES_BACKWARD_HALF: //Backwards half Speed event
    {
      setReverse();
      actuateLMotor(LHALF);
      actuateRMotor(RHALF);
    }
    break;
    
    case ES_CW: //Clockwise rotation
    {
      setCW();
      actuateLMotor(LFULL);
      actuateRMotor(RFULL);
    }
    break;
    
    case ES_CCW: //Clockwise rotation
    {
      setCCW();
      actuateLMotor(LFULL);
      actuateRMotor(RFULL);
    }
    break;
    
    case ES_ALIGN: //Clockwise rotation
    {
      setCCW();
      actuateLMotor(LFULL);
      actuateRMotor(RFULL);
    }
    break;

  }
  /*******************************************/
  return ReturnEvent;
}




bool CheckEvents (void) {
  // do nothing
  // getting error when the event list is empty
  return false;
}


/***************************************************************************
 private functions
 ***************************************************************************/
// Drive Modes and Directions
void setForward(void) {
  // set the directions forward
  forwardLeft = true;
  forwardRight = true;
  // set the polarity (both to HI)
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI); //Set forward polarity on 2A and 4A
}

void setReverse(void) {
  // set the directions forward
  forwardLeft = false;
  forwardRight = false;
  // set the polarity (both to LO)
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO); //Set reverse polarity on 2A and 4A
}

void setCW(void) {
  // set the directions forward
  forwardLeft = true;
  forwardRight = false;
  // set the polarity (left to HI, right to LO)
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT0HI;
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT1LO;
}

void setCCW(void) {
  // set the directions forward
  forwardLeft = false;
  forwardRight = true;
  // set the polarity (left to HI, right to LO)
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT0LO;
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT1HI;
}

void actuateLMotor(uint8_t DC) {
  // Left motor
  if (forwardLeft) {
    SetDuty(100 - DC, 0);
  }
  else {
    SetDuty(DC, 0);
  }
}

void actuateRMotor(uint8_t DC) {
  // Right motor
  if (forwardRight) {
    SetDuty(100 - DC, 1);
  }
  else {
    SetDuty(DC, 1);
  }
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

