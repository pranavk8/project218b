/****************************************************************************
 Module
   TemplateFSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"
#include "ButtonFSM.h"
#include "PINDEFS.h"
#include "StepService1.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/*----------------------------- Module Defines ----------------------------*/
// static ButtonState_t LastButtonState;
bool CheckButtonEvents(void);

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

uint8_t ReadInput(int PORT, uint8_t PINHI); 

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ButtonState_t CurrentState;
static uint8_t lastButton;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitButtonFSM(uint8_t Priority)
{
  HWREG(TOGGLE_PORT) |= ACTIVATE_PORT_F;
  while ((HWREG(TOGGLE_PORT) & (ACTIVATE_PORT_F)) != (ACTIVATE_PORT_F)) {
  }
  printf("\r\nActivated F ports.\r\n");
  
  // set the digital enable
  HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= BIT0HI;
  HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) &= BIT0LO;
  HWREG(GPIO_PORTF_BASE + GPIO_O_PUR) |= BIT0HI;
  
  // read the current state of the button 
  lastButton = ReadInput(GPIO_PORTF_BASE, BIT0HI);
  
  printf("%u", lastButton);
  
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = Debouncing;
  
  // start a timer that posts to button debounce SM
  ES_Timer_InitTimer(BUTTON_TIMER, 50); 
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostButtonFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunButtonFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case Ready2Sample: 
      if(ThisEvent.EventType == ES_BUTTON_UP) {
        ES_Timer_InitTimer(BUTTON_TIMER, 50); 
        CurrentState = Debouncing;
        ES_Event_t ThisEvent;
        // ThisEvent.EventType = ES_DB_BUTTON_UP;
        // PostButtonFSM(ThisEvent);
        ThisEvent.EventType = ES_DIRECTION_CHANGE;
        PostStepService1(ThisEvent);
        
      } else if(ThisEvent.EventType == ES_BUTTON_DOWN) {
        ES_Timer_InitTimer(BUTTON_TIMER, 50); 
        CurrentState = Debouncing;
        ES_Event_t ThisEvent;
        ThisEvent.EventType = ES_DB_BUTTON_DOWN;
        PostButtonFSM(ThisEvent);
      }
      break;
      
    case Debouncing:        // If current state is initial Psedudo State
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == BUTTON_TIMER) // and parameter is debounce timer number
      {
        CurrentState = Ready2Sample;
      }
    break;
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
ButtonState_t QueryButtonFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/

uint8_t ReadInput(int PORT, uint8_t PINHI) {
  return HWREG(PORT + (GPIO_O_DATA + ALL_BITS)) & PINHI;
}

bool CheckButtonEvents(void) {
  // read the current button
  static uint8_t currentButton;
  currentButton = ReadInput(GPIO_PORTF_BASE, BIT0HI);
  
  // printf("\n\rchecking button");

  if(lastButton != currentButton) {
    if(currentButton == 0) {
      // button low
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BUTTON_DOWN;
      ThisEvent.EventParam = 1;
      printf("\n\rbutt down");
      PostButtonFSM(ThisEvent);
      // post to the step service to change direction
    } else {
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ES_BUTTON_UP;
      ThisEvent.EventParam = 1;
      printf("\n\rbutt up");
      PostButtonFSM(ThisEvent);
    }
    lastButton = currentButton;
    return true;
  }
  return false;
}

