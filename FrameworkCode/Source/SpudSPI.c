/****************************************************************************
 Module
   SPI.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Events.h"
#include "SpudSPI.h"
#include "PWMLibrary.h"
#include "ADMulti.h"
#include "PINDEFS.h" // update this 
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"
#include "MiningOpsInfo.h"
#include "SensorsService.h"
#include "NormalOpsSM.h"
#include "MasterSM.h"
#include "ColorSensor.h"
#include "DriveSM.h"
/*----------------------------- Module Defines ----------------------------*/
#define SpudRequestTime 5

// Write Commands
#define BYTE_ZERO 0x00
#define STATUS 0xC0
#define C1MLOC1 0xC1
#define C1MLOC2 0xC2
#define C2MLOC1 0xC3
#define C2MLOC2 0xC4
#define C1RESH 0xC5
#define C1RESL 0xC6
#define C2RESH 0xC7
#define C2RESL 0xC8
#define PUR1 0xC9
#define PUR2 0xCA

// Team ID (move to masterSM once complete)

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void InitSpudSPI(void);
void RequestCommand(void);
void incrementIterator(void);
void SpudInfoMapper(uint8_t previousCommand);
void setTeamID(uint8_t TEAM);
void initMiners(void);
void updateMinerLocations(void);
void initTeamObjects(void);
void updatePermitLocs(void);
TeamObject_t getOurTeamObject(void);
TeamObject_t getOppTeamObject(void);
void updateTeamObjects(void);
void setSPUDTargetMINER_Index(uint8_t idx);
void setSPUDTargetTile(GridTile_t targetTile);
uint8_t getStatus(void);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t command;
static SpudState_t CurrentState;
static uint8_t commandCounter = 0;
static uint8_t commandIterator = 0;    // i = [0, 10]
static const uint8_t WriteCommandArray[11] = {STATUS, C1MLOC1, C1MLOC2, C2MLOC1, C2MLOC2, C1RESH, C1RESL, C2RESH, C2RESL, PUR1, PUR2};
static bool CheckForMinerFlag;
static Miner_t TargetMiner;
static GridTile_t TargetTile;
static uint8_t TargetIndex;

// info variables
static uint8_t teamID;
static uint8_t status;
static uint8_t c1mloc1;
static uint8_t c1mloc2;
static uint8_t c2mloc1;
static uint8_t c2mloc2;
static uint16_t c1reshi;
static uint16_t c1reslo;
static uint16_t c2reshi;
static uint16_t c2reslo;
static uint8_t c1permit;
static uint8_t c2permit;
static uint8_t neutral1permit;
static uint8_t neutral2permit;

// MINERS
static Miner_t Miner1;
static Miner_t Miner2;
static Miner_t Miner3;
static Miner_t Miner4;

// Team Objects
static TeamObject_t OurTeam, OppTeam;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitSpudSPIService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************/
  // init code for SPI
  
  /*******************************************/
  InitSpudSPI();
  printf("Spud Service Initialized!\r\n");
  InitTeamLEDStrip();
	InitTeamSwitch();
	SetTeam();
  initMiners();
  initTeamObjects();
  
  CheckForMinerFlag = false;
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostSpudSPIService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunSpudSPIService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
  ES_Event_t Event2Post;

  switch (CurrentState)
  {
    case InitSpudState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = WriteCommand;
				ES_Timer_InitTimer(SpudPrintTimer, 500);
        Event2Post.EventType = ES_SPUD_READY2WRITE;
        PostSpudSPIService(Event2Post);
      }
    }
    break;
    
    case WriteCommand:
    {
      if (ThisEvent.EventType == ES_SPUD_READY2WRITE)    // respond to whether or not we wrote a command
      {
        CurrentState = Wait;
        RequestCommand();    // write a command to the data register
        ES_Timer_InitTimer(SpudTimer, SpudRequestTime);    // start a 2 ms timer
//				printf("i: %d", commandIterator);
        incrementIterator();    // increment the iterator (only while writing good commands)
      }
    }
    break;
    
    case Wait:
    {
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SpudTimer)    // ready to write next command
      {
        // Only transition to write command once we've written 0x00 twice
        CurrentState = WriteCommand;
        Event2Post.EventType = ES_SPUD_READY2WRITE;
        PostSpudSPIService(Event2Post);
        // printf("Command Received from SPUD: %x\r\n", command);
//       printf("Permit Status from SPUD: %x\r\n", status);
      }
			
			if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SpudPrintTimer){
//					printf("Miner1 location: %d" , Miner1.currentLoc.tileColor.colorID);
//					printf("\tMiner2 location: %d\r\n" , Miner2.currentLoc.tileColor.colorID);
//					printf("Miner3 location: %d" , Miner3.currentLoc.tileColor.colorID);
//					printf("\tMiner4 location: %d\r\n" , Miner4.currentLoc.tileColor.colorID);
//					printf("\r\n");
//					printf("Permit location1: %d", c1permit);
//					printf("\tOpponent permit: %d\r\n" , c2permit);
//					printf("Neutral location1: %d", neutral1permit);
//					printf("\tNeutral location2: %d\r\n" , neutral2permit);
//					printf("\r\n");
//					printf("\r\n");
//				printf("Target Miner Index: %d\r\n" , TargetIndex);
//				printf("Target miner loc: %d\r\n", OurTeam.minerArray[TargetIndex].currentLoc.tileColor.colorID );
//				printf("TILE LOC: %d\r\n", TargetTile.tileColor.colorID);
					ES_Timer_InitTimer(SpudPrintTimer, 200);
					
		}
    }
    break;
     
  }
  /*******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
void InitSpudSPI(void){
  //  1. Enable the clock to the GPIO port 
  // Port A by default is SSI power on default 
  // PA2-5
  HWREG(SYSCTL_RCGCGPIO) |= (ACTIVATE_PORT_A);
  
  //  2. Enable the clock to SSI module
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;
  
  //  3. Wait for the GPIO port to be ready
  while ((HWREG(TOGGLE_PORT) & ACTIVATE_PORT_A) != ACTIVATE_PORT_A) {
  }
  
  //  4. Program the GPIO to use the alternate functions on the SSI pins
  // unnecessary since this is default for PA2-5
  
  //  5. Set mux position in GPIOPCTL to select the SSI use of the pins 
  uint8_t mux_val = 2; 
  // the  value being multiplied by bits per nibble is the pin on the port A 
  HWREG(GPIO_PORTA_BASE+GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) +
    (mux_val<<(2*BITS_PER_NYBBLE)) + (mux_val<<(3*BITS_PER_NYBBLE)) + 
      (mux_val<<(4*BITS_PER_NYBBLE)) + (mux_val<<(5*BITS_PER_NYBBLE));

  //  6. Program the port lines for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT5HI | BIT4HI | BIT3HI | BIT2HI);
  
  //  7. Program the required data directions on the port lines
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT5HI | BIT3HI | BIT2HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT4LO);
  
  //  8. If using SPI mode 3, program the pull-up on the clock line
  // enable the PUR on PA2 which is SSI0 SCK
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= BIT2HI; // enable pull up on SSI0 SCK
  
  //  9. Wait for the SSI0 to be ready
  while((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R0) != SYSCTL_RCGCSSI_R0){
  }
  
  //  10. Make sure that the SSI is disabled before programming mode bits
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;
  
  //  11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_MS;
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT;
  
  //  12. Configure the SSI clock source to the system clock
  HWREG(SSI0_BASE + SSI_O_CC) &= SSI_CC_CS_SYSPLL; 
  
  //  13. Configure the clock pre-scaler
  // target freq: 14925 HZ
  // step 1) set divisor = 16
  // step 2) solve for SCR (prescaler) and if it's in range,
  // shift it in the right location
  HWREG(SSI0_BASE + SSI_O_CPSR) = 16;
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & ~SSI_CR0_SCR_M) |
    (167 << SSI_CR0_SCR_S);
  
  
  //  14. Configure clock rate (SCR), phase & polarity (SPH, SPO), mode (FRF), data size (DSS)
  // for PCB generator (timing diagram)
  // Phase is 1 because writing data happens on even edges
  // Polarity is 1 because serial clock idles high
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH; // this is phase 1
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO; // this is polarity 1
  HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_FRF_M; 
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8; 
  
  //  15. Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

  //  16. Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
  //HWREG(SSI0_BASE + SSI_O_DR) = RequestCommandByte;
  //  17. Enable the NVIC interrupt for the SSI when starting to transmit
  //HWREG(NVIC_EN0) = BIT7HI;  //Interrupt number 7 for SSIO
  
    // make sure interrupts are enabled globally
  __enable_irq();
}

void incrementIterator(void) {
  if (commandIterator > 10) {
    commandIterator = 0;    // reset
  }
  else {
    commandIterator++;
  }
}

void RequestCommand(void) {
  
  // Write request byte to the data register
  HWREG(SSI0_BASE + SSI_O_DR) = WriteCommandArray[commandIterator];
  HWREG(SSI0_BASE + SSI_O_DR) = BYTE_ZERO;
  HWREG(SSI0_BASE + SSI_O_DR) = BYTE_ZERO;
  // Enable interrupts
  HWREG(NVIC_EN0) = BIT7HI;
  // printf("Command sent is %x\r\n", WriteCommandArray[commandIterator]);
}

void SpudInterrupt (void) {
  // Got an interrupt so disable it for now
  HWREG(NVIC_DIS0) = BIT7HI;
  
  // Read command
  //Ignore first two reads and get the message from third read
  HWREG(SSI0_BASE + SSI_O_DR);
  HWREG(SSI0_BASE + SSI_O_DR);
  //uint8_t newCommand = HWREG(SSI0_BASE + SSI_O_DR);
  command = HWREG(SSI0_BASE + SSI_O_DR);
  SpudInfoMapper(WriteCommandArray[commandIterator - 1]);
  updateMinerLocations();
  updatePermitLocs();
	updateTeamObjects();
}

void SpudInfoMapper(uint8_t previousCommand) {
  // switch the previous command sent to identify the register we're reading
switch (previousCommand)
  {
    // Write to the corresponding variables
    case STATUS:       
    {
      uint8_t newStatus = command & STATUS_READ_MASK;
      if (newStatus != status) {
        // Only post if there is a status change
        ES_Event_t Event2Post;
        if (newStatus == 0) {
          Event2Post.EventType = ES_WAITING_FOR_PERMITS;
        }
        else if (newStatus == 1) {
          Event2Post.EventType = ES_PERMITS_ISSUED;
        }
        else if (newStatus == 2) {
          Event2Post.EventType = ES_SUDDEN_DEATH;
        }
        else if (newStatus == 3) {
					setMiningOpsStatus(false);
          Event2Post.EventType = ES_PERMITS_EXPIRED;
					ES_Event_t E2P;
					E2P.EventType = ES_MASTER_STOP;
					PostDriveSM(E2P);
					setDriveDirection(0);
					SetDuty(0, 0);
					SetDuty(0, 1);
//					while(true){
//					}
        }
        // Post to MasterSM
        PostMasterSM(Event2Post);
        status = newStatus;    // Update status
      }
    }
    break;
    
    case C1MLOC1:       
    {
      // first check if miner is in good location
      if (command & MINER_LOC_STATUS_MASK) {
        c1mloc1 = (command & MINER_LOC_READ_MASK); // will assign number between 1 and 16
      }
      else {
        c1mloc1 = 16;
      }
    }
    break;
    
    case C1MLOC2:       
    {
      // first check if miner is in good location
      if (command & MINER_LOC_STATUS_MASK) {
        c1mloc2 = command & MINER_LOC_READ_MASK;
      }
      else {
        c1mloc2 = 16;
      }
    }
    break;
    
    case C2MLOC1:       
    {
      // first check if miner is in good location
      if (command & MINER_LOC_STATUS_MASK) {
        c2mloc1 = (command & MINER_LOC_READ_MASK); // will assign number between 1 and 16
      }
      else {
        c2mloc1 = 16;
      }
    }
    break;
    
    case C2MLOC2:       
    {
      // first check if miner is in good location
      if (command & MINER_LOC_STATUS_MASK) {
        c2mloc2 = command & MINER_LOC_READ_MASK;
      }
      else {
        c2mloc2 = 16;
      }
    }
    break;
    
    case C1RESH:       
    {
      c1reshi = command << 8;
    }
    break;
    
    case C1RESL:       
    {
      c1reslo = command;
    }
    break;
    
    case C2RESH:       
    {
      c2reshi = command << 8;
    }
    break;
    
    case C2RESL:       
    {
      c2reslo = command;
    }
    break;
    
    case PUR1:       
    {
      c1permit = command & PERMIT_C1_READ_MASK;
      c2permit = (command & PERMIT_C2_READ_MASK) >> 4;
    }
    break;
    
    case PUR2:       
    {
      neutral1permit = command & PERMIT_C1_READ_MASK;
      neutral2permit = (command & PERMIT_C2_READ_MASK) >> 4;
    }
    break;
  }
}

/***************************************************************************
 public functions
 ***************************************************************************/
// Sets the team status (call in InitMaster)
void setTeamID(uint8_t TEAM) {
  teamID = TEAM;
}

// Returns the location of our team's first miner
uint8_t getTeamMiner1Loc(void) {
  if (teamID == TEAM_CKH) {
    return c1mloc1;
  }
  else if (teamID == TEAM_GHI) {
    return c2mloc1;
  }
  // Error case: invalid team ID
  return 17;
}


// Returns the location of our team's second miner
uint8_t getTeamMiner2Loc(void) {
  if (teamID == TEAM_CKH) {
    return c1mloc2;
  }
  else if (teamID == TEAM_GHI) {
    return c2mloc2;
  }
  // Error case: invalid team ID
  return 17;
}


// Returns the target frequency of our team's first Miner
uint16_t getTeamMiner1Freq(void) {
  if (teamID == TEAM_CKH) {
    return 3333;
  }
  else if (teamID == TEAM_GHI) {
    return 1429;
  }
}

// Returns the target frequency of our team's second Miner
uint16_t getTeamMiner2Freq(void) {
  if (teamID == TEAM_CKH) {
    return 2000;
  }
  else if (teamID == TEAM_GHI) {
    return 909;
  }
}


// Returns our team's exclusive permit
uint8_t getTeamPermit(void) {
  if (teamID == TEAM_CKH) {
    return c1permit;
  }
  else if (teamID == TEAM_GHI) {
    return c2permit;
  }
}

// Returns neutral permit 1
uint8_t getNeutralPermit1(void) {
  return neutral1permit;
}

// Returns neutral permit 2
uint8_t getNeutralPermit2(void) {
  return neutral2permit;
}

void initMiners(void) {
  // MINER 1
  Miner1.team = TEAM_CKH;
  Miner1.freq = 3333;
  
  // MINER 2
  Miner2.team = TEAM_CKH;
  Miner2.freq = 2000;
  
  // MINER 3
  Miner3.team = TEAM_GHI;
  Miner3.freq = 1429;
  
  // MINER 4
  Miner4.team = TEAM_GHI;
  Miner4.freq = 909;
}

// Update miner objects
void updateMinerLocations(void) {
  // MINER 1
  if (teamID == TEAM_CKH){
    OurTeam.minerArray[0].currentLoc = getGridTile(c1mloc1);
    OurTeam.minerArray[1].currentLoc = getGridTile(c1mloc2);
    
    OppTeam.minerArray[0].currentLoc = getGridTile(c2mloc1);
    OppTeam.minerArray[1].currentLoc = getGridTile(c2mloc2);
  }
  
  else{
    OppTeam.minerArray[0].currentLoc = getGridTile(c1mloc1);
    OppTeam.minerArray[1].currentLoc = getGridTile(c1mloc2);
    
    OurTeam.minerArray[0].currentLoc = getGridTile(c2mloc1);
    OurTeam.minerArray[1].currentLoc = getGridTile(c2mloc2);
  }
  
  Miner1.currentLoc = getGridTile(c1mloc1);
  
  // MINER 2
  Miner2.currentLoc = getGridTile(c1mloc2);
  
  // MINER 3
  Miner3.currentLoc = getGridTile(c2mloc1);
  
  // MINER 4
  Miner4.currentLoc = getGridTile(c2mloc2);
}

void initTeamObjects(void) {
  if (teamID == TEAM_CKH) {
    OurTeam.minerArray[0] = Miner1;
    OurTeam.minerArray[1] = Miner2;
    OppTeam.minerArray[0] = Miner3;
    OppTeam.minerArray[1] = Miner4;
  } else {
    OurTeam.minerArray[0] = Miner3;
    OurTeam.minerArray[1] = Miner4;
    OppTeam.minerArray[0] = Miner1;
    OppTeam.minerArray[1] = Miner2;
  }
}

void updatePermitLocs(void) {
  if (teamID == TEAM_CKH) {
    OurTeam.permitLocs[0] = getGridTile(c1permit);
    OurTeam.permitLocs[1] = getGridTile(neutral1permit);
    OurTeam.permitLocs[2] = getGridTile(neutral2permit);
  } else {
    OurTeam.permitLocs[0] = getGridTile(c2permit);
    OurTeam.permitLocs[1] = getGridTile(neutral1permit);
    OurTeam.permitLocs[2] = getGridTile(neutral2permit);
  }
}

void updateTeamObjects(void) {
	if (teamID == TEAM_CKH) {
		OurTeam.minerArray[0].currentLoc = getGridTile(c1mloc1);
		OurTeam.minerArray[1].currentLoc = getGridTile(c1mloc2);
		OppTeam.minerArray[0].currentLoc = getGridTile(c2mloc1);
		OppTeam.minerArray[1].currentLoc = getGridTile(c2mloc1);
	}
	else {
		OurTeam.minerArray[0].currentLoc = getGridTile(c2mloc1);
		OurTeam.minerArray[1].currentLoc = getGridTile(c2mloc2);
		OppTeam.minerArray[0].currentLoc = getGridTile(c1mloc1);
		OppTeam.minerArray[1].currentLoc = getGridTile(c1mloc1);
	}
}

TeamObject_t getOurTeamObject(void) {
  return OurTeam;
}

TeamObject_t getOppTeamObject(void) {
  return OppTeam;
}

void resetStatus(void) {
  status = 0;
}

uint8_t getStatus(void) {
  return status ;
}

bool checkIfMinerReached(void){
	
  if (CheckForMinerFlag){
//		printf("Curr miner loc on deposit is %d\r\n", getTargetMiner().currentLoc.tileColor.colorID);
//		printf("Target tile is %d \r\n", getTargetTile().tileColor.colorID);
//		Color_t currCol = getCurrentColor();
//		GridTile_t currLoc = getCurrentTile(currCol);
//    if (TargetMiner.currentLoc.tileColor.colorID == getTargetTile().tileColor.colorID){
		
//		printf("Target miner locin event checker: %d\r\n", OurTeam.minerArray[TargetIndex].currentLoc.tileColor.colorID );
    if (OurTeam.minerArray[TargetIndex].currentLoc.tileColor.colorID == TargetTile.tileColor.colorID){
      ES_Event_t Event2Post;
			setEM(false);
      Event2Post.EventType = MINER_REACHED_DESTINATION;
      PostMasterSM(Event2Post);
      CheckForMinerFlag = false;
			printf("Spud reached destination\r\n");
//			Event2Post.EventType = ES_MASTER_STOP;
//			PostDriveSM(Event2Post);
//			// Set beam break checker OFF
//			setBeamBreak(false);
//			Event2Post.EventType = ES_REVERSE;
//			Event2Post.EventParam = 100;    // Need to tune this
//			PostDriveSM(Event2Post);
			
      return true;
    }
    return false;
  }
  return false;
}

void setCheckMinerFlag(bool flag){
  CheckForMinerFlag = flag;
}

//void setSPUDTargetMINER(Miner_t targetMiner) {
void setSPUDTargetMINER_Index(uint8_t idx) {
	TargetIndex = idx;
}

void setSPUDTargetTile(GridTile_t targetTile) {
	TargetTile = targetTile;
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

