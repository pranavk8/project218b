/****************************************************************************
 Module
   RobotFSM.c

 Revision
   1.0.1

 Description
   This is a Robot file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunRobotSM()
 10/23/11 18:20 jec      began conversion from SMRobot.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "RobotFSM.h"
#include "DCMotorService.h"
#include "ADMulti.h"

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <float.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/
#define rotateTimer45 1000
#define rotateTimer90 2000
#define rotateTimerAlign 5000
#define tapeThreshold 1000 // tune this!
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
void stopMotors(void);
void activateIR(bool on);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static RobotState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
static bool tapeDetected;
static uint8_t LastIRState;
static uint16_t timeStart;
static uint16_t numRiseEdges = 0;
static bool isFirstEdge = true;
static bool detectIR = false;
static uint8_t tapeCount = 0;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitRobotFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitRobotFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitPState;
  // post the initial transition event
  
  // Initialize hardware
  // 1. Tape sensor
  ADC_MultiInit(1); // activates PE0;
  // 2. IR sensor
 // Digital enable
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI);
  // Set direction to input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT0LO | BIT1LO | BIT2LO);
  
  // No tape detected initially
  tapeDetected = false;
  
  stopMotors();
  
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostRobotFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostRobotFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunRobotFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunRobotFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
  ES_Event_t Event2Post;

  switch (CurrentState)
  {
    case InitPState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = Stationary;
      }
    }
    break;

    case Stationary:        // If current state is state one
    {
      if (tapeDetected) {
        printf("Tape detected! Ignorng commands...\r\n");
        stopMotors();
        return ReturnEvent;
      }
      switch (ThisEvent.EventType)
      {
        case ES_FORWARD_FULL:   

        { 
          printf("ES_FORWARD_FULL\r\n");
          CurrentState = Translate;   
          Event2Post.EventType = ES_FORWARD_FULL;
          PostDCMotorService(Event2Post);
        }
        break;
        
        case ES_FORWARD_HALF:   

        { 
          printf("ES_FORWARD_HALF\r\n");
          CurrentState = Translate;   
          Event2Post.EventType = ES_FORWARD_HALF;
          PostDCMotorService(Event2Post);
        }
        break;
        
        case ES_BACKWARD_FULL:   

        { 
          printf("ES_BACKWARD_FULL\r\n");
          CurrentState = Translate;   
          Event2Post.EventType = ES_BACKWARD_FULL;
          PostDCMotorService(Event2Post);
        }
        break;
        
        case ES_BACKWARD_HALF:   

        { 
          printf("ES_BACKWARD_HALF\r\n");
          CurrentState = Translate;   
          Event2Post.EventType = ES_BACKWARD_HALF;
          PostDCMotorService(Event2Post);
        }
        break;
        
        case ES_CW_45:   

        {
          printf("ES_CW_45\r\n");
          CurrentState = Rotate;   
          Event2Post.EventType = ES_CW;
          PostDCMotorService(Event2Post);
          ES_Timer_InitTimer(MotionTimer, rotateTimer45);
        }
        break;
        
        case ES_CCW_45:   

        {
          printf("ES_CCW_45\r\n");
          CurrentState = Rotate;   
          Event2Post.EventType = ES_CCW;
          PostDCMotorService(Event2Post);
          ES_Timer_InitTimer(MotionTimer, rotateTimer45);
        }
        break;
        
        case ES_CW_90:   

        {
          printf("ES_CW_90\r\n");
          CurrentState = Rotate;   
          Event2Post.EventType = ES_CW;
          PostDCMotorService(Event2Post);
          ES_Timer_InitTimer(MotionTimer, rotateTimer90);
        }
        break;
        
        case ES_CCW_90:   

        {
          printf("ES_CCW_90\r\n");
          CurrentState = Rotate;   
          Event2Post.EventType = ES_CCW;
          PostDCMotorService(Event2Post);
          ES_Timer_InitTimer(MotionTimer, rotateTimer90);
        }
        break;
        
        case ES_ALIGN:

        {
          printf("ES_ALIGN\r\n");
          CurrentState = Align;   
          Event2Post.EventType = ES_ALIGN;
          PostDCMotorService(Event2Post);
          ES_Timer_InitTimer(MotionTimer, rotateTimerAlign);
          activateIR(true);
        }
        break;
        
        case ES_DRIVE_TIL_TAPE:

        {
          printf("ES_DRIVE_TIL_TAPE\r\n");
          CurrentState = Translate;   
          Event2Post.EventType = ES_FORWARD_FULL;
          PostDCMotorService(Event2Post);
          
        }
        break;
        // repeat cases as required for relevant events
        default:
          ;
      }  // end switch on CurrentEvent
    }
    break;
    
    case Translate:
    {
      if (ThisEvent.EventType == ES_STOP) {
        stopMotors();
      }
      else if (ThisEvent.EventType == ES_TAPE_DETECTED) {
        // Set a flag after which we ignore commands
        tapeDetected = true;
        stopMotors();
      }
    }
    break;
    
    
    case Rotate:
    {
      if (ThisEvent.EventType == ES_STOP) {
        stopMotors();
      }
      
      else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == MotionTimer) {
        // finished rotating
        stopMotors();
      }
      
      else if (ThisEvent.EventType == ES_TAPE_DETECTED) {
        // Set a flag after which we ignore commands
        tapeDetected = true;
        stopMotors();
      }
    }
    break;
    
    case Align:
    {
      if (ThisEvent.EventType == ES_STOP) {
        stopMotors();
        activateIR(false);
      }
      
      else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == MotionTimer) {
        // finished rotating
        stopMotors();
        activateIR(false);
      }
      
      else if (ThisEvent.EventType == ES_TAPE_DETECTED) {
        // Set a flag after which we ignore commands
        tapeDetected = true;
        stopMotors();
        activateIR(false);
      }
      
      else if (ThisEvent.EventType == ES_IR_DETECTED) {
        printf("Detected IR");
        stopMotors();
        activateIR(false);
      }
    }
    break;
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryRobotSM

 Parameters
     None

 Returns
     RobotState_t The current state of the Robot state machine

 Description
     returns the current state of the Robot state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
RobotState_t QueryRobotFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
uint16_t readAnalogInput(void) {
  uint32_t result[1];
  ADC_MultiRead(result);
  return result[0];
  }

void stopMotors(void) {
  ES_Event_t NewEvent;
  printf("ES_STOP\r\n");
  NewEvent.EventType = ES_STOP;
  PostDCMotorService(NewEvent);
  CurrentState = Stationary;
}  
  

uint8_t ReadInput(int PORT, uint8_t PINHI) {
  // printf("%X\r\n", HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT3HI);
  return HWREG(PORT + (GPIO_O_DATA + ALL_BITS)) & PINHI;
}

// Event Checkers

bool tapeDetector(void) {
  // read PE0
  uint16_t tapeADCount = readAnalogInput();
  // printf("Tape value: %d\r\n", tapeADCount);
  // If its below a threshold. return true
  if(tapeADCount < tapeThreshold){
    tapeCount++;
   
  }
  else{
    // reset
    tapeCount = 0;
  }
  if (tapeCount > 20) {
    tapeDetected = true;
    printf("Tape detected \r\n");
    ES_Event_t E2P;
    E2P.EventType = ES_STOP;
    PostRobotFSM(E2P);
    return true;
  }
  else {
    return false;
    }
}

bool IRDetector (void) {
  // printf("Checking hit\r\n");
  if (!detectIR) {
    numRiseEdges = 0;
    isFirstEdge = true;
    return false;
  }
  // printf("Checking hit\r\n");
  uint8_t CurrentIRState = ReadInput(GPIO_PORTB_BASE, BIT2HI);
  if (CurrentIRState != LastIRState) {
    LastIRState = CurrentIRState;
    if (CurrentIRState) {
      if (isFirstEdge) {
        timeStart = ES_Timer_GetTime();
        numRiseEdges++;
        isFirstEdge = false;
      } else {
        numRiseEdges++; 
        uint16_t dT = ES_Timer_GetTime() - timeStart;
        if (numRiseEdges >= 30 && (dT) < 250) {
          // valid hit (IR frequency is greater than 1200 Hz);
          ES_Event_t Event2Post;
          Event2Post.EventType = ES_IR_DETECTED;
          PostRobotFSM(Event2Post);
          printf("Correct IR frequency picked up\r\n");
          numRiseEdges = 0;
          isFirstEdge = true;
          return true;
        }
        else if (dT > 250) {
          // didnt get enough edges in 250 ms so reset
          numRiseEdges = 0;
          isFirstEdge = true;
        }
      }
    }
  }
  return false;
}

void activateIR(bool on) {
  detectIR = on;
}
