/****************************************************************************
 Module
   EventCheckers.c

 Revision
   1.0.1

 Description
   This is the sample for writing event checkers along with the event
   checkers used in the basic framework test harness.

 Notes
   Note the use of static variables in sample event checker to detect
   ONLY transitions.

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 13:36 jec     initial version
****************************************************************************/

// this will pull in the symbolic definitions for events, which we will want
// to post in response to detecting events
#include "MiningOpsInfo.h"
#include "ES_Configure.h"
// This gets us the prototype for ES_PostAll
#include "ES_Framework.h"
// this will get us the structure definition for events, which we will need
// in order to post events in response to detecting events
#include "ES_Events.h"
// if you want to use distribution lists then you need those function
// definitions too.
#include "ES_PostList.h"
// This include will pull in all of the headers from the service modules
// providing the prototypes for all of the post functions
#include "ES_ServiceHeaders.h"
// this test harness for the framework references the serial routines that
// are defined in ES_Port.c
#include "ES_Port.h"
// include our own prototypes to insure consistency between header &
// actual functionsdefinition
#include "EventCheckers.h"

#include "PWMLibrary.h"

// This is the event checking function sample. It is not intended to be
// included in the module. It is only here as a sample to guide you in writing
// your own event checkers
#if 0
/****************************************************************************
 Function
   Check4Lock
 Parameters
   None
 Returns
   bool: true if a new event was detected
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Lock(void)
{
  static uint8_t  LastPinState = 0;
  uint8_t         CurrentPinState;
  bool            ReturnVal = false;

  CurrentPinState = LOCK_PIN;
  // check for pin high AND different from last time
  // do the check for difference first so that you don't bother with a test
  // of a port/variable that is not going to matter, since it hasn't changed
  if ((CurrentPinState != LastPinState) &&
      (CurrentPinState == LOCK_PIN_HI)) // event detected, so post detected event
  {
    ES_Event ThisEvent;
    ThisEvent.EventType   = ES_LOCK;
    ThisEvent.EventParam  = 1;
    // this could be any of the service post functions, ES_PostListx or
    // ES_PostAll functions
    ES_PostAll(ThisEvent);
    ReturnVal = true;
  }
  LastPinState = CurrentPinState; // update the state for next time

  return ReturnVal;
}

#endif

/****************************************************************************
 Function
   Check4Keystroke
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 Notes
   The functions that actually check the serial hardware for characters
   and retrieve them are assumed to be in ES_Port.c
   Since we always retrieve the keystroke when we detect it, thus clearing the
   hardware flag that indicates that a new key is ready this event checker
   will only generate events on the arrival of new characters, even though we
   do not internally keep track of the last keystroke that we retrieved.
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Keystroke(void)
{
  ES_Event_t KeyEvent;
  
  if (IsNewKeyReady())   // new key waiting?
  {
    ES_Event_t ThisEvent;
    ThisEvent.EventType   = ES_NEW_KEY;
    ThisEvent.EventParam  = GetNewKey();
    
    switch (ThisEvent.EventParam)
    {
      case 's':
      {
        KeyEvent.EventType = ES_EMERGENCY_STOP;
        printf("Key pressed, should stop\r\n");
        PostDriveSM(KeyEvent);
				//PostMasterSM(KeyEvent);
      }
      break;
      
      case 'w':
      {
        KeyEvent.EventType = ES_FORWARD;
        printf("Key pressed, should go forward\r\n");
        KeyEvent.EventParam = 605;
        PostDriveSM(KeyEvent);
      }
      break;
      
      case 'a':
      {
        KeyEvent.EventType = ES_ROTATE_CCW;
        printf("Key pressed, should turn CCW\r\n");
        KeyEvent.EventParam = 30;
        PostDriveSM(KeyEvent);
      }
      break;
      
      case 'd':
      {
        KeyEvent.EventType = ES_ROTATE_CW;
        printf("Key pressed, should turn CW\r\n");
        KeyEvent.EventParam = 30;
        PostDriveSM(KeyEvent);
      }
      break;
			
			case 'h':
      {
        KeyEvent.EventType = ES_SPIRAL_FORWARD;
        printf("Key pressed, should SPIRAL\r\n");
        KeyEvent.EventParam = 300;
        PostDriveSM(KeyEvent);
      }
      break;
      
      case 'x':
      {
        KeyEvent.EventType = ES_REVERSE;
        printf("Key pressed, should go reverse\r\n");
        KeyEvent.EventParam = 605;
        PostDriveSM(KeyEvent);
      }
      break;
      
      case 'p':
      {
        KeyEvent.EventType = ES_PERMITS_ISSUED;
        PostMasterSM(KeyEvent);
      }
      break;
      
      case 'i':
      {
        KeyEvent.EventType = ES_IR_DETECTED;
        PostMasterSM(KeyEvent);
      }
      break;
      
      case 'b':
      {
        KeyEvent.EventType = ES_BEAM_BROKEN;
        PostMasterSM(KeyEvent);
      }
      break;
      
      case 'r':
      {
        KeyEvent.EventType = ES_BEAM_RESTORED;
        PostMasterSM(KeyEvent);
      }
      break;
      
			case 'm':
      {
        KeyEvent.EventType = MINER_REACHED_DESTINATION;
        PostMasterSM(KeyEvent);
      }
      break;
			
			case 'o':
      {
        KeyEvent.EventType = ES_PERMITS_EXPIRED;
        PostMasterSM(KeyEvent);
				setDriveDirection(0);
					SetDuty(0, 0);
					SetDuty(0, 1);
					while(true){
					}
      }
      break;
			
      case 'z':
      {
				ES_Event_t NewEvent;
				uint8_t targetTileIdx = 3;
				Color_t currentColor = getCurrentColor();
				
        GridTile_t currentTile = getCurrentTile(currentColor);
				printf("Current tile: %d\r\n", currentTile.tileColor.colorID);
        GridTile_t targetTile = getGridTile(targetTileIdx);
				int16_t TargetAngle, TargetDist;
				TargetAngle = amountToRotate(currentTile, targetTile);
				TargetDist = computeTileDistance(currentTile, targetTile);
				
				if (TargetAngle > 0) {
					if(TargetAngle > 180){
						NewEvent.EventType = ES_ROTATE_CW;
						NewEvent.EventParam = 360 - TargetAngle;
						printf("CLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
					}
					else{
            NewEvent.EventType = ES_ROTATE_CCW;
						NewEvent.EventParam = TargetAngle;
						printf("COUNTERCLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
					}
        }
        else {
					if (TargetAngle > -180){
						NewEvent.EventType = ES_ROTATE_CW;
						NewEvent.EventParam = abs(TargetAngle);
						printf("CLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
          }
					else{
						NewEvent.EventType = ES_ROTATE_CCW;
						NewEvent.EventParam = 360 - abs(TargetAngle);
						printf("COUNTERCLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
					}
				}
					
          printf("Aligning with goal\r\n");
          PostDriveSM(NewEvent);
					printf("Distance for TILE 6 %d\r\n" , TargetDist);
					printf("\r\n");
      }
      break;
      
      case 'q':
      {
        printf("Heading is: %d\r\n", GetCurrentHeading());
//        SetGetHeading(false);
      }
      break;
    }
    return true;
  }
  return false;
  
}

