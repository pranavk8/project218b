/****************************************************************************
 Module
   DriveSM.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Hardware.h"
#include "PWMLibrary.h"
#include "DriveSM.h"
#include "EncoderService.h"
#include "DummyMasterFSM.h"
#include "MasterSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define radiusRotation 121; // mm
#define FORWARD 0
#define REVERSE 1
#define CLOCKWISE 2
#define COUNTERCLOCKWISE 3

#define HIGH_SPEED 35 // rpm
#define LOW_SPEED 40 // rpm
#define SPIRAL_SPEED 10 // rpm
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
void setLeftMotorDirection(uint8_t direction);
void setRightMotorDirection(uint8_t direction);
void setDriveDirection(uint8_t direction);
double convertAngleToDistance(double theta_degrees);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static DriveState_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;
uint32_t PWMPeriod = 300;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDriveSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitDriveSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitPState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  
  // Initialize the PWM Library (done here because only the motors use PWM)
  InitPWMLibrary(2, PWMPeriod, 0, 0, 0, 0, 0);
  
  // Activate ports that control motor polarity
  activatePort(PORT_B);
  
  // Enable digital on polarity pins
  setPinDigital(PORT_LEFT_MOTOR, PIN_LEFT_MOTOR_POLARITY);
  setPinDigital(PORT_RIGHT_MOTOR, PIN_RIGHT_MOTOR_POLARITY);
  
  // Set polarity pins direction to output
  setPinDirection(PORT_LEFT_MOTOR, PIN_LEFT_MOTOR_POLARITY, OUTPUT);
  setPinDirection(PORT_RIGHT_MOTOR, PIN_RIGHT_MOTOR_POLARITY, OUTPUT);
   
  
  setDriveDirection(Forward);
  // Set Target RPM on Both Motors
  setTargetRightMotorRPM(0);
  setTargetLeftMotorRPM(0);
  printf("init drivesm\r\n");
//  CurrentState = Forward;
//  setDriveDirection(COUNTERCLOCKWISE);
//  setTargetRightMotorRPM(HIGH_SPEED);
//  setTargetLeftMotorRPM(HIGH_SPEED);
  
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostDriveSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDriveSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDriveSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDriveSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

	if (ThisEvent.EventType == ES_EMERGENCY_STOP){
		setTargetRightMotorRPM(0);
    setTargetLeftMotorRPM(0);
		while(true){
		}
	}
  switch (CurrentState)
  {
    case InitPState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        CurrentState = Stopped;
        // Turn off the motors
        setTargetRightMotorRPM(0);
        setTargetLeftMotorRPM(0);
      }
    }
    break;

    case Stopped:        // If current state is state one
    {
      if (ThisEvent.EventType == ES_FORWARD) {
        CurrentState = Forward;
        setDriveDirection(FORWARD);
        setTargetRightMotorRPM(HIGH_SPEED);
        setTargetLeftMotorRPM(HIGH_SPEED);
        setTargetLeftMotorEncoderCount(ThisEvent.EventParam);
        setTargetRightMotorEncoderCount(ThisEvent.EventParam);
      }
      
      if (ThisEvent.EventType == ES_REVERSE) {
        CurrentState = Reverse;
        setDriveDirection(REVERSE);
        setTargetRightMotorRPM(HIGH_SPEED);
        setTargetLeftMotorRPM(HIGH_SPEED);
        setTargetLeftMotorEncoderCount(ThisEvent.EventParam);
        setTargetRightMotorEncoderCount(ThisEvent.EventParam);
      }
      
      if (ThisEvent.EventType == ES_ROTATE_CW) {
        CurrentState = Rotating;
        setDriveDirection(CLOCKWISE);
        setTargetRightMotorRPM(LOW_SPEED);
        setTargetLeftMotorRPM(LOW_SPEED);
        setTargetLeftMotorEncoderCount(convertAngleToDistance(ThisEvent.EventParam));
        setTargetRightMotorEncoderCount(convertAngleToDistance(ThisEvent.EventParam));
      }
      
      if (ThisEvent.EventType == ES_ROTATE_CCW) {
        CurrentState = Rotating;
        setDriveDirection(COUNTERCLOCKWISE);
        setTargetRightMotorRPM(LOW_SPEED);
        setTargetLeftMotorRPM(LOW_SPEED);
        setTargetLeftMotorEncoderCount(convertAngleToDistance(ThisEvent.EventParam));
        setTargetRightMotorEncoderCount(convertAngleToDistance(ThisEvent.EventParam));
      }
      
      if (ThisEvent.EventType == ES_SPIRAL_FORWARD) {
        CurrentState = Spiralling;
        setDriveDirection(FORWARD);
        setTargetRightMotorRPM(70);
        setTargetLeftMotorRPM(40);
        setTargetLeftMotorEncoderCount(ThisEvent.EventParam);
        setTargetRightMotorEncoderCount(ThisEvent.EventParam);
      }
      
      if (ThisEvent.EventType == ES_SPIRAL_REVERSE) {
        CurrentState = Spiralling;
        setDriveDirection(REVERSE);
        setTargetRightMotorRPM(SPIRAL_SPEED);
        setTargetLeftMotorRPM(LOW_SPEED);
        setTargetLeftMotorEncoderCount(convertAngleToDistance(ThisEvent.EventParam));
        setTargetRightMotorEncoderCount(convertAngleToDistance(ThisEvent.EventParam));
      }
      // Back up timer in case we fail 
//      ES_Timer_InitTimer(DriveSMTimer, 8000);
    }
    break;
    
    
    case Forward:
    {
     if (ThisEvent.EventType == ES_STOP) {
       CurrentState = Stopped;
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       if (QueryMasterSM() == CollisionOps) {
         // this was a collision response
         Event2Post.EventType = COLLISION_RESPONSE_COMPLETE;
         PostMasterSM(Event2Post);
         printf("Collision resp complete\r\n");
       }
       else {
         // default case: normalops
         Event2Post.EventType = ES_MOTION_COMPLETE;
         Event2Post.EventParam = MC_FORWARD;
         PostMasterSM(Event2Post);
       }
     }
     
     else if (ThisEvent.EventType == ES_MASTER_STOP) {
       CurrentState = Stopped;
        setDriveDirection(FORWARD);
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
     }
     
     else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DriveSMTimer) {
       CurrentState = Stopped;
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       Event2Post.EventType = ES_MOTION_COMPLETE;
       Event2Post.EventParam = MC_FORWARD;
       PostMasterSM(Event2Post);
     }
    }
    break;
    
    
    case Reverse:
    {
      if (ThisEvent.EventType == ES_STOP) {
       CurrentState = Stopped;
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       if (QueryMasterSM() == CollisionOps) {
         // this was a collision response
         Event2Post.EventType = COLLISION_RESPONSE_COMPLETE;
         PostMasterSM(Event2Post);
         printf("Collision resp complete\r\n");
       }
       else {
         // default case: normalops
         Event2Post.EventType = ES_MOTION_COMPLETE;
         Event2Post.EventParam = MC_REVERSE;
         PostMasterSM(Event2Post);
       }
     }
     
     else if (ThisEvent.EventType == ES_MASTER_STOP) {
       CurrentState = Stopped;
        setDriveDirection(FORWARD);
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
     }
     
     
     else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DriveSMTimer) {
       CurrentState = Stopped;
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       
       if (QueryMasterSM() == CollisionOps) {
         // this was a collision response
         Event2Post.EventType = COLLISION_RESPONSE_COMPLETE;
         PostMasterSM(Event2Post);
         printf("Collision resp complete\r\n");
       }
       else {
         Event2Post.EventType = ES_MOTION_COMPLETE;
       Event2Post.EventParam = MC_REVERSE;
       PostMasterSM(Event2Post);
       }
     }
    }
    break;
    
    
    case Rotating:
    {
      if (ThisEvent.EventType == ES_STOP) {
       CurrentState = Stopped;
        setDriveDirection(FORWARD);
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       Event2Post.EventType = ES_MOTION_COMPLETE;
       Event2Post.EventParam = MC_ROTATION;
       PostMasterSM(Event2Post);
     }
      
     else if (ThisEvent.EventType == ES_MASTER_STOP) {
       CurrentState = Stopped;
        setDriveDirection(FORWARD);
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
     }
     else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DriveSMTimer) {
       CurrentState = Stopped;
       setDriveDirection(FORWARD);
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       Event2Post.EventType = ES_MOTION_COMPLETE;
       Event2Post.EventParam = MC_ROTATION;
       PostMasterSM(Event2Post);
     }
    }
    break;
    
    
    case Spiralling:
    {
      if (ThisEvent.EventType == ES_STOP) {
       CurrentState = Stopped;
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       Event2Post.EventType = ES_MOTION_COMPLETE;
       Event2Post.EventParam = MC_SPIRALLING;
       PostMasterSM(Event2Post);
     }
      
     else if (ThisEvent.EventType == ES_MASTER_STOP) {
       CurrentState = Stopped;
        setDriveDirection(FORWARD);
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
     }
     
     else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == DriveSMTimer) {
       CurrentState = Stopped;
       setTargetRightMotorRPM(0);
       setTargetLeftMotorRPM(0);
       ES_Event_t Event2Post;
       Event2Post.EventType = ES_MOTION_COMPLETE;
       Event2Post.EventParam = MC_SPIRALLING;
       PostMasterSM(Event2Post);
     }
    }
    break;
    // repeat state pattern as required for other states
    default:
      ;
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     DriveState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
DriveState_t QueryDriveSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
// Control Direction of Robot
void setDriveDirection(uint8_t direction) {
  if (direction == FORWARD) {
    setLeftMotorDirection(FORWARD);
    setRightMotorDirection(FORWARD);
  }
  else if (direction == REVERSE) {
    setLeftMotorDirection(REVERSE);
    setRightMotorDirection(REVERSE);
  }
  else if (direction == CLOCKWISE) {
    setLeftMotorDirection(FORWARD);
    setRightMotorDirection(REVERSE);
  }
  else if (direction == COUNTERCLOCKWISE) {
    setLeftMotorDirection(REVERSE);
    setRightMotorDirection(FORWARD);
  }
}


// Control Direction for individual motors
void setLeftMotorDirection(uint8_t direction) {
  if (direction == FORWARD) {
    setLeftForward(true);
    writeDigitalOutput(PORT_LEFT_MOTOR, PIN_LEFT_MOTOR_POLARITY, LO);
  }
  else if (direction == REVERSE) {
    setLeftForward(false);
    writeDigitalOutput(PORT_LEFT_MOTOR, PIN_LEFT_MOTOR_POLARITY, HI);
  }
}


void setRightMotorDirection(uint8_t direction) {
  if (direction == FORWARD) {
    setRightForward(true);
    writeDigitalOutput(PORT_RIGHT_MOTOR, PIN_RIGHT_MOTOR_POLARITY, LO);
  }
  else if (direction == REVERSE) {
    setRightForward(false);
    writeDigitalOutput(PORT_RIGHT_MOTOR, PIN_RIGHT_MOTOR_POLARITY, HI);
  }
}

double convertAngleToDistance(double theta_degrees) {
  double theta_radians = (double) (theta_degrees * 3.1415 / 180.0);
  return theta_radians * radiusRotation;
}
