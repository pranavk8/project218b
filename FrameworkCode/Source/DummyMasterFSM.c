/****************************************************************************
 Module
   DummyMasterFSM.c

 Revision
   1.0.1

 Description
   This is a DummyMaster file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunDummyMasterSM()
 10/23/11 18:20 jec      began conversion from SMDummyMaster.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "bitdefs.h"


// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "ADService.h"
#include "PWMLibrary.h"
#include "Hardware.h"

#include "DummyMasterFSM.h"
#include "DriveSM.h"
#include "SensorsService.h"
#include "MiningOpsInfo.h"
#include "ColorSensor.h"
#include "AccelSPIFSM.h"
/*----------------------------- Module Defines ----------------------------*/
#define ON true
#define OFF false
#define TARGET_FREQ_VAL 3333  
#define BeaconWaitTime 1000
#define targetTileIdx 13

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
uint16_t getTargetFrequency(void);
int16_t amountToRotate(GridTile_t currentTile, GridTile_t targetTile);
void GenerateAndPostRotateEvent(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static DummyMasterState_t CurrentState;
static uint16_t TargetFrequency, CurrentHeading;
static bool IsFirstRun, IsIRDetected, IsMinerPickedUp, IsAlignedToGoal, IsFirstTimeout;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDummyMasterFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool InitDummyMasterFSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  initializeWorld();
  CurrentState = InitDummyMasterState;
  IsFirstRun = true;
  IsIRDetected = false;
  IsAlignedToGoal = false;
  IsMinerPickedUp = false;
  IsFirstTimeout = true;
  
  setIRChecker(OFF);
  setEM(OFF);
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostDummyMasterFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostDummyMasterFSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDummyMasterFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunDummyMasterFSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent, NewEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  
  switch (CurrentState)
  {
    case InitDummyMasterState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
        // this is where you would put any actions associated with the
        // transition from the initial pseudo-state into the actual
        // initial state

        // now put the machine into the actual initial state
        
        CurrentState = WaitingForSpud;
        
      }
    }
    break;
    
    case WaitingForSpud:
    {
      if (ThisEvent.EventType == ES_PERMITS_ISSUED && IsFirstRun == true){
        IsFirstRun = false;
        printf("Permits have issued\r\n");
        CurrentState = Aligning;
        TargetFrequency = TARGET_FREQ_VAL;
        setIRChecker(ON);
        NewEvent.EventType = ES_ROTATE_CW;
        printf("Aligning with beacon, turning CW\r\n");
        NewEvent.EventParam = 359;
        PostDriveSM(NewEvent);
      }
    }
    break;

    case Aligning:        // If current state is state one
    {
      if (ThisEvent.EventType == ES_IR_DETECTED){
        NewEvent.EventType = ES_STOP;
        IsIRDetected = true;
        printf("Beacon found\r\n");
        PostDriveSM(NewEvent);
        setIRChecker(OFF);
        ES_Timer_InitTimer(PauseTimer, BeaconWaitTime);
      }
      
      if(!IsIRDetected && ThisEvent.EventType == ES_MOTION_COMPLETE && ThisEvent.EventParam == MC_ROTATION){
        printf("Failed to find beacon\r\n");
        NewEvent.EventType = ES_ROTATE_CW;
        printf("Aligning with beacon, turning CW again!\r\n");
        NewEvent.EventParam = 359;
        PostDriveSM(NewEvent);
      }
      
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == PauseTimer){
        CurrentState = NavToMiner;
        NewEvent.EventType = ES_FORWARD;
        printf("Driving to miner\r\n");
        NewEvent.EventParam = 500;
        PostDriveSM(NewEvent);
      }
    }
    break;
    
    case NavToMiner:
    {
      if (!IsMinerPickedUp && ThisEvent.EventType == ES_MOTION_COMPLETE && ThisEvent.EventParam == MC_FORWARD){
        printf("Encoder ticks expired, miner not found\r\n");
        IsIRDetected = false;
        CurrentState = Aligning;
        setIRChecker(ON);
        NewEvent.EventType = ES_ROTATE_CW;
        printf("Aligning with beacon, turning CW\r\n");
        NewEvent.EventParam = 359;
        PostDriveSM(NewEvent);
      }
      
      if(ThisEvent.EventType == ES_BEAM_BROKEN){
        printf("Miner picked up\r\n");
        setEM(ON);
        IsMinerPickedUp = true;
        NewEvent.EventType = ES_STOP;
        PostDriveSM(NewEvent);
        ES_Timer_InitTimer(PauseTimer, BeaconWaitTime);
        IsFirstTimeout = true;
        
      }
      
      if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == PauseTimer){
//        if(!IsFirstTimeout) {
//          CurrentHeading = GetCurrentHeading();
//          CurrentState = NavToGoal;
//          Color_t currentColor = getCurrentColor();
//          GridTile_t currentTile = getCurrentTile(currentColor);
//          GridTile_t targetTile = getGridTile(targetTileIdx);
//          int16_t rot =  amountToRotate(currentTile, targetTile);
//          if (rot >= 0) {
//            NewEvent.EventType = ES_ROTATE_CW;
//          }
//          else {
//            NewEvent.EventType = ES_ROTATE_CCW;
//          }
//          printf("Aligning with goal\r\n");
//          NewEvent.EventParam = rot;
//          PostDriveSM(NewEvent);
//          IsFirstTimeout = true;
//        }
//        else {
//          IsFirstTimeout = false;
//          ES_Timer_InitTimer(PauseTimer, BeaconWaitTime);
//        }
        // should do its thing
        GenerateAndPostRotateEvent();
        CurrentState = NavToGoal;
      }
    }
    break;
    
    case NavToGoal:
    {
      if (ThisEvent.EventType == ES_MOTION_COMPLETE && ThisEvent.EventParam == MC_ROTATION){
        printf("Aligned with goal\r\n");
        IsAlignedToGoal = true;
        NewEvent.EventType = ES_STOP;
        PostDriveSM(NewEvent);
        Color_t currentColor = getCurrentColor();
        GridTile_t currentTile = getCurrentTile(currentColor);
        GridTile_t targetTile = getGridTile(targetTileIdx);
        NewEvent.EventType = ES_FORWARD;
        NewEvent.EventParam = computeTileDistance(currentTile, targetTile);
        PostDriveSM(NewEvent);
        printf("Driving to goal\r\n");
      }
      
      if (ThisEvent.EventType == ES_BEAM_RESTORED){
        printf("Miner lost\r\n");
        IsAlignedToGoal = false;
        IsIRDetected = false;
        IsMinerPickedUp = false;
        CurrentState = Aligning;
        setIRChecker(ON);
        NewEvent.EventType = ES_STOP;
        PostDriveSM(NewEvent);
        NewEvent.EventType = ES_ROTATE_CW;
        printf("Aligning with beacon, turning CW\r\n");
        NewEvent.EventParam = 359;
        PostDriveSM(NewEvent);
      }
      
      if (IsAlignedToGoal && ThisEvent.EventType == ES_MOTION_COMPLETE && ThisEvent.EventParam == MC_FORWARD){
        // will have to change this
        setEM(OFF);
        printf("Miner reached destination \r\n");
        CurrentState = WaitingForSpud;
        NewEvent.EventType = ES_STOP;
        PostDriveSM(NewEvent);
      }
      
    }
    break;
    
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State

  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryDummyMasterSM

 Parameters
     None

 Returns
     DummyMasterState_t The current state of the DummyMaster state machine

 Description
     returns the current state of the DummyMaster state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
DummyMasterState_t QueryDummyMasterFSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
uint16_t getTargetFrequency(void){
  return TargetFrequency;
}

int16_t amountToRotate(GridTile_t currentTile, GridTile_t targetTile) {
  return computeAngularDifference(currentTile, targetTile) - GetCurrentHeading();
}


// Function posts a rotate event to align the robot to the goal
void GenerateAndPostRotateEvent(void) {
  ES_Event_t NewEvent;
  
  // read current tile color
  Color_t currentColor = getCurrentColor();
  // use color to get the current tile
  GridTile_t currentTile = getCurrentTile(currentColor);
  printf("Current tile: %d\r\n", currentTile.tileColor.colorID);
  
  // get the target tile (this should be set in the planning state)
  GridTile_t targetTile = getGridTile(targetTileIdx);
  int16_t TargetAngle, TargetDist;
  
  // compute how much you want to rotate based on current heading and target heading
  TargetAngle = amountToRotate(currentTile, targetTile);
  TargetDist = computeTileDistance(currentTile, targetTile);
  
  // rotate in the right direction
  if (TargetAngle > 0) {
    if(TargetAngle > 180){
      NewEvent.EventType = ES_ROTATE_CW;
      NewEvent.EventParam = 360 - TargetAngle;
      printf("CLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
    else{
      NewEvent.EventType = ES_ROTATE_CCW;
      NewEvent.EventParam = TargetAngle;
      printf("COUNTERCLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
  }
  else if (TargetAngle < 0) {
    if (TargetAngle > -180){
      NewEvent.EventType = ES_ROTATE_CW;
      NewEvent.EventParam = abs(TargetAngle);
      printf("CLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
    else{
      NewEvent.EventType = ES_ROTATE_CCW;
      NewEvent.EventParam = 360 - abs(TargetAngle);
      printf("COUNTERCLOCKWISE by Angle: %d\r\n" , NewEvent.EventParam);
    }
  }
  else {
    NewEvent.EventType = ES_ROTATE_CCW;
    NewEvent.EventParam = 0;
  }
    
    printf("Aligning with goal\r\n");
  // post the correct event to driveSM
    PostDriveSM(NewEvent);
    printf("Distance for TILE 6 %d\r\n" , TargetDist);
    printf("\r\n");
}