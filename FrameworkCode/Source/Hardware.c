/****************************************************************************
 Module
   Hardware.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Hardware.h"

/***************************************************************************
 Hardware Initialization
 ***************************************************************************/



/***************************************************************************
 helper functions
 ***************************************************************************/
// Turns on a specific port
 void activatePort(uint32_t portName) {
   HWREG(SYSCTL_RCGCGPIO) |= portName;
   while ((HWREG(TOGGLE_PORT) & portName) != portName) {
   }
 }


// Sets a particular pin on a port to digital (use ADCMulti for analog stuff)
void setPinDigital(uint32_t portName, uint8_t pinNum) {
  if (pinNum > 7) {
    printf("Error [setPinDigital]: Pin out of range.\r\n");
  }
  HWREG(portName + GPIO_O_DEN) |= 1 << pinNum;
}


// Configures a pin to be input/output
void setPinDirection(uint32_t portName, uint8_t pinNum, uint8_t mode) {
 if (pinNum > 7) {
   printf("Error [setPinDirection]: Pin out of range.\r\n");
 }
 else {
   if (mode == OUTPUT) {
     HWREG(portName + GPIO_O_DIR) |= 1 << pinNum;
   }
   else if (mode == INPUT) {
     HWREG(portName + GPIO_O_DIR) &= ~(1 << pinNum);
   }
 }
}

// Configures a pin to be open drain
void setPinOpenDrain(uint32_t portName, uint8_t pinNum) {
 if (pinNum > 7) {
   printf("Error [setPinDirection]: Pin out of range.\r\n");
 }
 else {
   HWREG(portName + GPIO_O_ODR) |= 1 << pinNum;
 }
}

// Activates the pull-up resistor on a pin
void activatePinPullUp(uint32_t portName, uint8_t pinNum) {
  if (pinNum > 7) {
    printf("Error [activatePinPullUp]: Pin out of range.\r\n");
  }
  HWREG(portName + GPIO_O_PUR) |= 1 << pinNum;
}


// Reads the state of a pin
uint8_t readDigitalInput(uint32_t portName, uint8_t pinNum) {
  if (pinNum > 7) {
    printf("Error [readDigitalInput]: Pin out of range.\r\n");
    return ~0;
  }
  return HWREG(portName + (GPIO_O_DATA + ALL_BITS)) & (1 << pinNum);
}


// Writes HI/LO to a pin on a port
void writeDigitalOutput(uint32_t portName, uint8_t pinNum, uint8_t value) {
  if (pinNum > 7) {
    printf("Error [writeDigitalOutput]: Pin out of range.\r\n");
  }
  else {
    if (value) {
      // Set bit
      HWREG(portName + (GPIO_O_DATA + ALL_BITS)) |= 1 << pinNum;
    }
    else {
      // Clear bit
      HWREG(portName + (GPIO_O_DATA + ALL_BITS)) &= ~(1 << pinNum);
    }
  }
}

// Deprecated Functions
// // Sets a particular pin on a port to analog
// void setPinAnalog(uint8_t portName, uint8_t pinLO) {
//   HWREG(portName + GPIO_O_DEN) &= pinLO
// }
//
//
// // Configures a pin to be an output
// void setPinOutput(uint8_t portName, uint8_t pinHI) {
//   HWREG(portName + GPIO_O_DIR) |= pinHI
// }
//
//
// // Configures a pin to be an input
// void setPinInput(uint8_t portName, uint8_t pinLO) {
//   HWREG(portName + GPIO_O_DIR) &= pinLO
// }
//
//
// // Writes HI to an output pin
// void writeDigitalOutputHI(uint8_t portName, uint8_t pinHI) {
//   HWREG(portName + (GPIO_O_DATA + ALL_BITS)) |= pinHI
// }
//
//
// // Writes LO to an output pin
// void writeDigitalOutputLO(uint8_t portName, uint8_t pinLO) {
//   HWREG(portName + (GPIO_O_DATA + ALL_BITS)) &= pinLO
// }
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
