#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"
#include "MiningOpsInfo.h"
#include <math.h>

// Module defines
#define tileLength 610

// Function Prototypes
float normColor(Color_t color1, Color_t color2);
void initColors(void);
void initGridMap(void);
GridTile_t getCurrentTile(Color_t currentColor);

// Module variables
static GridTile_t GridMap[17];    // Stores all the tiles in the world
static Color_t ColorList[17];

// Color
static Color_t COLOR_LAVENDER;
static Color_t COLOR_MAROON;
static Color_t COLOR_BLUE;
static Color_t COLOR_MINT;
static Color_t COLOR_OLIVE;
static Color_t COLOR_BEIGE;
static Color_t COLOR_ORANGE;
static Color_t COLOR_MAGENTA;
static Color_t COLOR_APRICOT;
static Color_t COLOR_RED;
static Color_t COLOR_PURPLE;
static Color_t COLOR_CYAN;
static Color_t COLOR_NAVY;
static Color_t COLOR_LIME;
static Color_t COLOR_BROWN;
static Color_t COLOR_YELLOW;
static Color_t COLOR_WHITE;

// arc tan look up table
uint16_t ArcTanLookUpTable[7][7] = 
{
  {225,214,198,180,162,146,135},
  {236,225,207,180,153,135,124},
  {252,243,225,180,135,117,108},
  {270,270,270,0,90,90,90},
  {288,297,315,0,45,63,72},
  {304,315,333,0,27,45,56},
  {315,326,342,0,18,34,45}
};


// Functions
/****************************************************************************/
// Initialize the world (call in Master)
void initializeWorld(void) {
  // initialize color data
  initColors();
  printf("Colors Initialized\r\n");
  // initialize grid tiles
  initGridMap();
  printf("Map Initialized\r\n");
}
/****************************************************************************/
uint16_t computeAngularDifference(GridTile_t currentTile, GridTile_t targetTile) {
  // does some look up stuff
  int dX = (currentTile.center_x - targetTile.center_x) / tileLength;
  int dY = (currentTile.center_y - targetTile.center_y) / tileLength;
  return ArcTanLookUpTable[dX + 3][dY + 3];
}

uint16_t computeTileDistance(GridTile_t currentTile, GridTile_t targetTile) {
  // does some look up stuff
  return sqrt(pow((currentTile.center_x - targetTile.center_x), 2) + pow((currentTile.center_y - targetTile.center_y), 2));
}

// Returns the target tile
GridTile_t getGridTile(uint8_t tileColorOrIndex) {
  return GridMap[tileColorOrIndex];
}


// Fills in color data
void initColors(void) {
  
  // Lavender
  COLOR_LAVENDER.colorID = LAVENDER;
  COLOR_LAVENDER.cl = 12490;
  COLOR_LAVENDER.R = 35.66;
  COLOR_LAVENDER.G = 30.95;
  COLOR_LAVENDER.B = 31.62;
  
  // MAROON
  COLOR_MAROON.colorID = MAROON;
  COLOR_MAROON.cl = 3590;
  COLOR_MAROON.R = 49.10;
  COLOR_MAROON.G = 27.04;
  COLOR_MAROON.B = 24.51;
  
  // BLUE
  COLOR_BLUE.colorID = BLUE;
  COLOR_BLUE.cl = 6011;
  COLOR_BLUE.R = 24.52;
  COLOR_BLUE.G = 34.02;
  COLOR_BLUE.B = 41.33;
  
  // MINT
  COLOR_MINT.colorID = MINT;
  COLOR_MINT.cl = 11800;
  COLOR_MINT.R = 33.41;
  COLOR_MINT.G = 37.08;
  COLOR_MINT.B = 27.27;
  
  // OLIVE
  COLOR_OLIVE.colorID = OLIVE;
  COLOR_OLIVE.cl = 5307;
  COLOR_OLIVE.R = 39.84;
  COLOR_OLIVE.G = 35.10;
  COLOR_OLIVE.B = 24.08;
  
  // BEIGE
  COLOR_BEIGE.colorID = BEIGE;
  COLOR_BEIGE.cl = 17410;
  COLOR_BEIGE.R = 37.69;
  COLOR_BEIGE.G = 34.72;
  COLOR_BEIGE.B = 24.92;
  
  // ORANGE
  COLOR_ORANGE.colorID = ORANGE;
  COLOR_ORANGE.cl = 4466;
  COLOR_ORANGE.R = 56.32;
  COLOR_ORANGE.G = 25.47;
  COLOR_ORANGE.B = 18.48;
  
  // MAGENTA
  COLOR_MAGENTA.colorID = MAGENTA;
  COLOR_MAGENTA.cl = 5756;
  COLOR_MAGENTA.R = 46.36;
  COLOR_MAGENTA.G = 25.10;
  COLOR_MAGENTA.B = 29.22;
  
  // APRICOT
  COLOR_APRICOT.colorID = APRICOT;
  COLOR_APRICOT.cl = 14475;
  COLOR_APRICOT.R = 42.38;
  COLOR_APRICOT.G = 31.76;
  COLOR_APRICOT.B = 23.85;
  
  // RED
  COLOR_RED.colorID = RED;
  COLOR_RED.cl = 7118;
  COLOR_RED.R = 61.69;
  COLOR_RED.G = 21.12;
  COLOR_RED.B = 18.96;
  
  // PURPLE
  COLOR_PURPLE.colorID = PURPLE;
  COLOR_PURPLE.cl = 4677;
  COLOR_PURPLE.R = 39.26;
  COLOR_PURPLE.G = 27.22;
  COLOR_PURPLE.B = 34.07;
  
  // CYAN
  COLOR_CYAN.colorID = CYAN;
  COLOR_CYAN.cl = 10250;
  COLOR_CYAN.R = 25.80;
  COLOR_CYAN.G = 37.32;
  COLOR_CYAN.B = 35.19;
  
  // NAVY
  COLOR_NAVY.colorID = NAVY;
  COLOR_NAVY.cl = 3690;
  COLOR_NAVY.R = 33.71;
  COLOR_NAVY.G = 30.90;
  COLOR_NAVY.B = 35.56;
  
  // LIME
  COLOR_LIME.colorID = LIME;
  COLOR_LIME.cl = 11225;
  COLOR_LIME.R = 40.60;
  COLOR_LIME.G = 37.65;
  COLOR_LIME.B = 19.27;
  
  // BROWN
  COLOR_BROWN.colorID = BROWN;
  COLOR_BROWN.cl = 5680;
  COLOR_BROWN.R = 47.79;
  COLOR_BROWN.G = 29.86;
  COLOR_BROWN.B = 21.89;
  
  // YELLOW
  COLOR_YELLOW.colorID = YELLOW;
  COLOR_YELLOW.cl = 13213;
  COLOR_YELLOW.R = 45.90;
  COLOR_YELLOW.G = 34.39;
  COLOR_YELLOW.B = 17.34;
  
  // WHITE
  COLOR_WHITE.colorID = WHITE;
  COLOR_WHITE.cl = 0;
  COLOR_WHITE.R = 35.00;
  COLOR_WHITE.G = 33.37;
  COLOR_WHITE.B = 28.60;
  
  // Add all these colors to the list
  ColorList[0] = COLOR_LAVENDER;
  ColorList[1] = COLOR_MAROON;
  ColorList[2] = COLOR_BLUE;
  ColorList[3] = COLOR_MINT;
  ColorList[4] = COLOR_OLIVE;
  ColorList[5] = COLOR_BEIGE;
  ColorList[6] = COLOR_ORANGE;
  ColorList[7] = COLOR_MAGENTA;
  ColorList[8] = COLOR_APRICOT;
  ColorList[9] = COLOR_RED;
  ColorList[10] = COLOR_PURPLE;
  ColorList[11] = COLOR_CYAN;
  ColorList[12] = COLOR_NAVY;
  ColorList[13] = COLOR_LIME;
  ColorList[14] = COLOR_BROWN;
  ColorList[15] = COLOR_YELLOW;
  ColorList[16] = COLOR_WHITE;
}

void initGridMap(void){
  // Initialize all the tiles in the grid
  int i = 0;
  // Loop over all rows
  for (int r = 0; r < 4; r++) {
    // Loop over all columns
    for (int c = 0; c < 4; c++) {
      GridMap[i].tileColor = ColorList[i];    // Maps the tile color
      GridMap[i].center_x = r * tileLength;   // Set the x pos
      GridMap[i].center_y = c * tileLength;   // Set the y pos
      i++;    // Update index
    }
  }
  GridMap[i].tileColor = ColorList[i];    // If we detect white
  GridMap[i].center_x = 10 * tileLength;   // invalid x
  GridMap[i].center_y = 10 * tileLength;   // invalid y
}


// Pass in measurement from the color sensor, and find which tile you are on
GridTile_t getCurrentTile(Color_t currentColor) {
  // scan through colors to find closest
  uint16_t minVal = 65535;    // max 16bit val
  int minIdx = 0;
  for (int i = 0; i < 17; i++) {
    Color_t color2compare = ColorList[i];
    float norm = normColor(currentColor, color2compare);
//    printf("norm: %8.4f\r\n", norm);
//    printf("norm: %d\r\n", minVal);
//    printf("minidx: %d\r\n", minIdx);
    // update min
    if (norm < minVal) {
      minIdx = i;
      minVal = norm;
    }
  }
//  printf("norm: %d\r\n", minVal);
  return GridMap[minIdx];
}


// Finds the color norm
float normColor(Color_t color1, Color_t color2) {
  float dR = (color1.R - color2.R) * (color1.R - color2.R);
  float dG = (color1.G - color2.G) * (color1.G - color2.G);
  float dB = (color1.B - color2.B) * (color1.B - color2.B);
  return sqrt(dR + dG + dB);
}

// Computes the color
Color_t computeColor(Color_t measuredColor) {
	// scan through colors to find closest
  uint16_t minVal = 65535;    // max 16bit val
  int minIdx = 0;
  for (int i = 0; i < 17; i++) {
    Color_t color2compare = ColorList[i];
    float norm = normColor(measuredColor, color2compare);
//    printf("norm: %8.4f\r\n", norm);
//    printf("norm: %d\r\n", minVal);
//    printf("minidx: %d\r\n", minIdx);
    // update min
    if (norm < minVal) {
      minIdx = i;
      minVal = norm;
    }
  }
//  printf("norm: %d\r\n", minVal);
  return ColorList[minIdx];
}