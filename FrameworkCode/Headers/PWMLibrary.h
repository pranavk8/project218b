/****************************************************************************

  Header file for PWM service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef LibPWM_H
#define LibPWM_H

#include "ES_Types.h"

// Public Function Prototypes

void InitPWMLibrary(uint8_t num, uint32_t period0, uint32_t duty0, uint32_t duty1, uint32_t period1, uint32_t duty2, uint32_t duty3);
void SetPeriod(uint32_t PeriodMicroS, uint8_t group);
void SetDuty(uint32_t duty, uint8_t channel);

#endif /* ServPWM_H */

