// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.h"

#ifndef HARDWARE_H
#define HARDWARE_H
/***********************************************************************/
// Hardware Macros
#define TOGGLE_PORT SYSCTL_RCGCGPIO
#define PORT_A BIT0HI
#define PORT_B BIT1HI
#define PORT_C BIT2HI
#define PORT_D BIT3HI
#define PORT_E BIT4HI
#define PORT_F BIT5HI

#define DIGITAL 0
#define ANALOG 1
#define OUTPUT 0
#define INPUT 1
#define LO 0
#define HI 1

/***********************************************************************/
// Subsystems
/***********************************************************************/
// Motors
// Left Motor
#define PORT_LEFT_MOTOR GPIO_PORTB_BASE
#define PIN_LEFT_MOTOR_POLARITY 0

// Right Motor
#define PORT_RIGHT_MOTOR GPIO_PORTB_BASE
#define PIN_RIGHT_MOTOR_POLARITY 1
// *PWM functions handled by PWM Library; default to
// PB6 and PB7
/**************************************************************************/
// IR
# define PORT_IR GPIO_PORTC_BASE
# define PIN_IR 7
/**************************************************************************/
// Team ID
// Switch
 #define PORT_TEAM_ID_SWITCH GPIO_PORTE_BASE
 #define PIN_TEAM_ID_SWITCH 3

// LEDs
 #define PORT_TEAM_LED_RED GPIO_PORTE_BASE
 #define PORT_TEAM_LED_BLUE GPIO_PORTE_BASE
 #define PIN_TEAM_LED_RED 5
 #define PIN_TEAM_LED_BLUE 4
 
 // Mining In Porgess LED
 #define PORT_MIP_LED GPIO_PORTB_BASE
 #define PIN_MIP_LED 4
/**************************************************************************/
// EM Subsystem
#define PORT_EM_SYSTEM GPIO_PORTB_BASE
#define PIN_EM_SYSTEM 5
/**************************************************************************/
// Beam Break System
#define PORT_BEAM_BREAK GPIO_PORTA_BASE
#define PIN_BEAM_BREAK 6
/**************************************************************************/
// Limit Switches
#define PORT_LIMIT_SWITCH GPIO_PORTE_BASE
#define PIN_LIMIT_SWITCH 1
/**************************************************************************/
/* TIMERS */
#define SPUD_REQUEST_TIME 2 // 2 ms

/**************************************************************************/
// Public Functions


// Helper Functions
void activatePort(uint32_t portName);
void setPinDigital(uint32_t portName, uint8_t pinNum);
void setPinDirection(uint32_t portName, uint8_t pinNum, uint8_t mode);
void setPinOpenDrain(uint32_t portName, uint8_t pinNum);
void activatePinPullUp(uint32_t portName, uint8_t pinNum);
uint8_t readDigitalInput(uint32_t portName, uint8_t pinNum);
void writeDigitalOutput(uint32_t portName, uint8_t pinNum, uint8_t value);

// Deprecated Functions
// void setPinDigital(uint8_t portName, uint8_t pinHI);
// void setPinAnalog(uint8_t portName, uint8_t pinLO);
// void setPinOutput(uint8_t portName, uint8_t pinHI);
// void setPinInput(uint8_t portName, uint8_t pinLO);
// void activatePinPullUp(uint8_t portName, uint8_t pinHI);
// uint8_t readDigitalInput(uint8_t portName, uint8_t pinHI);
// void writeDigitalOutputHI(uint8_t portName, uint8_t pinHI);
// void writeDigitalOutputLO(uint8_t portName, uint8_t pinLO);

#endif
