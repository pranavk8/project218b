/****************************************************************************
 Navigating header file for Hierarchical Sate Machines AKA StateCharts
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef HSMNavigating_H
#define HSMNavigating_H


// typedefs for the states
// State definitions for use with the query function
typedef enum { NavRest, Align2Goal, Driving2Goal,
 NavExploring, Reversing} NavigatingState_t ;


// Public Function Prototypes

ES_Event_t RunNavigatingSM( ES_Event_t CurrentEvent );
void StartNavigatingSM ( ES_Event_t CurrentEvent );
NavigatingState_t QueryNavigatingSM ( void );

#endif /*SHMNavigating_H */

