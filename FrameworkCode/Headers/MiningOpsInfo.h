/* Definitions that map the state of mining operations */
//typedef enum
//{
//  STATUS_WAITING_FOR_PERMITS,
//  STATUS_PERMITS_ISSUED,
//  STATUS_SUDDEN_DEATH,
//  STATUS_PERMITS_EXPIRED,
//  
//} WorldState_t;

#ifndef MINING_OPS_H
#define MINING_OPS_H


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"

// Masks
#define STATUS_READ_MASK (BIT0HI | BIT1HI)
#define MINER_LOC_STATUS_MASK BIT7HI
#define MINER_LOC_READ_MASK (BIT0HI | BIT1HI | BIT2HI | BIT3HI)
#define PERMIT_C1_READ_MASK (BIT0HI | BIT1HI | BIT2HI | BIT3HI)
#define PERMIT_C2_READ_MASK ~(BIT0HI | BIT1HI | BIT2HI | BIT3HI)


// Colors
#define LAVENDER 0
#define MAROON 1
#define BLUE 2
#define MINT 3
#define OLIVE 4
#define BEIGE 5
#define ORANGE 6
#define MAGENTA 7
#define APRICOT 8
#define RED 9
#define PURPLE 10
#define CYAN 11
#define NAVY 12
#define LIME 13
#define BROWN 14
#define YELLOW 15
#define WHITE 16;
#define UNKNOWN 17;

// Color
typedef struct Color {
  uint8_t colorID;
  uint16_t cl;
  float R, G, B;
} Color_t;


// Grid Tile
typedef struct GridTile {
  Color_t tileColor;
  float center_x;
  float center_y;
} GridTile_t;


// Miner
typedef struct Miner {
  uint8_t team;    // either CKH or GHI
  uint16_t freq;    // IR freq
  GridTile_t currentLoc;    // Location
} Miner_t;

// Team Objects
typedef struct TeamObject {
  Miner_t minerArray[2];
  GridTile_t permitLocs[3];
} TeamObject_t;


// Public Functions
void initializeWorld(void);
GridTile_t getCurrentTile(Color_t currentColor);
uint16_t computeAngularDifference(GridTile_t currentTile, GridTile_t targetTile);
GridTile_t getGridTile(uint8_t tileColorOrIndex);
uint16_t computeTileDistance(GridTile_t currentTile, GridTile_t targetTile);
Color_t computeColor(Color_t measuredColor);
#endif