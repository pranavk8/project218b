/****************************************************************************

  Header file for Sensors service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServSensors_H
#define ServSensors_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitSensorsService(uint8_t Priority);
bool PostSensorsService(ES_Event_t ThisEvent);
ES_Event_t RunSensorsService(ES_Event_t ThisEvent);
void setIRChecker(bool on_off);
void setEM(bool on_off);
void setMiningOpsStatus(bool on_off);
bool BeamBreakEventChecker(void);
bool LimitSwitchEventChecker(void);

void InitTeamLEDStrip(void);
void InitTeamSwitch(void);
void initLimitSwitch(void);
void SetTeam(void);
void setBeamBreak(bool on_off);
void setTargetFrequency(uint16_t freq);

#endif /* ServSensors_H */

