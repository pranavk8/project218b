/****************************************************************************

  Header file for Test Harness I2C Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ColorSensor_H
#define ColorSensor_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"
#include "MiningOpsInfo.h"

//Defines for different colors to pass in params
//#define NO_COL 17
//#define WHITE 18
//#define UNKNOWN_COL 17
//#define NAVY 1
//#define LIME 2
//#define BROWN 3
//#define YELLOW 4
//#define APRICOT 5
//#define RED 6
//#define PURPLE 7
//#define CYAN 8
//#define OLIVE 9
//#define BEIGE 10
//#define ORANGE 11
//#define MAGENTA 12
//#define LAVENDER 13
//#define MAROON 14
//#define BLUE 15
//#define MINT 16


// Public Function Prototypes

bool InitColorSensor(uint8_t Priority);
bool PostColorSensor(ES_Event_t ThisEvent);
ES_Event_t RunColorSensor(ES_Event_t ThisEvent);
Color_t getCurrentColor(void);
#endif /* ColorSensor_H */

