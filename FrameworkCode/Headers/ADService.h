/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ADService_H
#define ADService_H

#include "ES_Types.h"
#include "ES_Events.h"

// Public Function Prototypes

bool InitADService(uint8_t Priority);
bool PostADService(ES_Event_t ThisEvent);
ES_Event_t RunADService(ES_Event_t ThisEvent);

int16_t readAnalogInput(void);
bool CheckPotReading(void);
uint32_t returnTargetRPM(void);
uint32_t returnTargetDC(void);

#endif /* ServTemplate_H */

