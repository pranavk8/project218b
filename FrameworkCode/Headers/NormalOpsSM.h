/****************************************************************************
 NormalOps header file for Hierarchical Sate Machines AKA StateCharts
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef NormalOpsSM_H
#define NormalOpsSM_H
#include "MiningOpsInfo.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { WaitingForPermits, Planning, Navigating } NormalOpsState_t ;


// Public Function Prototypes

ES_Event_t RunNormalOpsSM( ES_Event_t CurrentEvent );
void StartNormalOpsSM ( ES_Event_t CurrentEvent );
NormalOpsState_t QueryNormalOpsSM ( void );
bool GetIfMinerIsPickedUp(void);
GridTile_t getTargetTile(void);
Miner_t getTargetMiner(void);
void SetMinerPickedUp(bool flag);
GridTile_t getCurrLoc(void);
void MakeDecision(void);
#endif /*SHMNormalOps_H */

