/****************************************************************************

  Header file for DummyMaster Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMDummyMaster_H
#define FSMDummyMaster_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "MiningOpsInfo.h"
// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitDummyMasterState, WaitingForSpud, Aligning, NavToMiner,
  NavToGoal
}DummyMasterState_t;

// Public Function Prototypes

bool InitDummyMasterFSM(uint8_t Priority);
bool PostDummyMasterFSM(ES_Event_t ThisEvent);
ES_Event_t RunDummyMasterFSM(ES_Event_t ThisEvent);
DummyMasterState_t QueryDummyMasterSM(void);
uint16_t getTargetFrequency(void);

#endif /* FSMDummyMaster_H */

