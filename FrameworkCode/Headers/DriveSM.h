/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef DriveSM_H
#define DriveSM_H

#define MC_FORWARD 1
#define MC_REVERSE 2
#define MC_ROTATION 3
#define MC_SPIRALLING 4

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitPState, Stopped, Forward,
  Reverse, Rotating, Spiralling
}DriveState_t;

// Public Function Prototypes

bool InitDriveSM(uint8_t Priority);
bool PostDriveSM(ES_Event_t ThisEvent);
ES_Event_t RunDriveSM(ES_Event_t ThisEvent);
DriveState_t QueryDriveSM(void);
void setDriveDirection(uint8_t direction);

#endif /* DriveSM_H */

