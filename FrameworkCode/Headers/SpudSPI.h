/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef SpudSPI_H
#define SpudSPI_H

#include "ES_Types.h"
#include "ES_Events.h"
#include "MiningOpsInfo.h"

// Public Function Prototypes

typedef enum
{
  InitSpudState, WriteCommand, Wait //, Write0
}SpudState_t;
#define TEAM_CKH 0
#define TEAM_GHI 1

bool InitSpudSPIService(uint8_t Priority);
bool PostSpudSPIService(ES_Event_t ThisEvent);
ES_Event_t RunSpudSPIService(ES_Event_t ThisEvent);
void setTeamID(uint8_t TEAM);
TeamObject_t getOurTeamObject(void);
TeamObject_t getOppTeamObject(void);
void resetStatus(void);
void setCheckMinerFlag(bool flag);
bool checkIfMinerReached(void);
void setSPUDTargetMINER_Index(uint8_t idx);
void setSPUDTargetTile(GridTile_t targetTile);
uint8_t getStatus(void);
#endif /* ServTemplate_H */

