/****************************************************************************

  Header file for AccelSPI Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMAccelSPI_H
#define FSMAccelSPI_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitAccelSPIState, Initializing, Waiting, Active
}AccelSPIState_t;

// Public Function Prototypes

bool InitAccelSPIFSM(uint8_t Priority);
bool PostAccelSPIFSM(ES_Event_t ThisEvent);
ES_Event_t RunAccelSPIFSM(ES_Event_t ThisEvent);
AccelSPIState_t QueryAccelSPISM(void);
void SetGetHeading(bool flag);
uint16_t GetCurrentHeading(void);
  
#endif /* FSMAccelSPI_H */

