/****************************************************************************
 Planning header file for Hierarchical Sate Machines AKA StateCharts
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef Planning_H
#define Planning_H


// typedefs for the states
// State definitions for use with the query function
typedef enum { Rest, Exploring } PlanningState_t ;


// Public Function Prototypes

ES_Event_t RunPlanningSM( ES_Event_t CurrentEvent );
void StartPlanningSM ( ES_Event_t CurrentEvent );
PlanningState_t QueryPlanningSM ( void );
void setMakeDecisionFlag(bool flag);
bool CheckMakeDecision(void);
#endif /*SHMPlanning_H */

