/****************************************************************************

  Header file for Encoder service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServEncoder_H
#define ServEncoder_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitEncoderService(uint8_t Priority);
bool PostEncoderService(ES_Event_t ThisEvent);
ES_Event_t RunEncoderService(ES_Event_t ThisEvent);
void setTargetLeftMotorRPM(uint8_t speed);
void setTargetLeftMotorEncoderCount(float distanceMM);
void setTargetRightMotorRPM(uint8_t speed);
void setTargetRightMotorEncoderCount(float distanceMM);
void setLeftForward(bool isForward);
void setRightForward(bool isForward);
#endif /* ServEncoder_H */
