/****************************************************************************
 Template header file for Hierarchical Sate Machines AKA StateCharts

 ****************************************************************************/

#ifndef MasterSM_H
#define MasterSM_H

// State definitions for use with the query function
typedef enum { NormalOps, CollisionOps} MasterState_t ;

// Public Function Prototypes

ES_Event_t RunMasterSM( ES_Event_t CurrentEvent );
void StartMasterSM ( ES_Event_t CurrentEvent );
bool PostMasterSM( ES_Event_t ThisEvent );
bool InitMasterSM ( uint8_t Priority );
MasterState_t QueryMasterSM(void);
#endif /*MasterSM_H */

