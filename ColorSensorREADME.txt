Project for ColorSensor is in Keni's folder and is named ColorSensorCode (pranavk8/ColorSensorCode/FrameworkCode)

I2CService is given by Ed, not to be messed with.
ColorSensor.c written to poll the values from the color sensor and check which color is
picked up by the sensor.
Color Sensor.c needs to be edited to include all colors (presently written only for RED).
Present idea of posting to MasterSM is with different EventParams for each color

Steps to include color sensor into our project:
1. Add I2Cservice source and header files to project.
2. Add ColorSensor.c and .h to project.
3. In ES_CONFIGURE:
	a) increment the number of services by 2
	b) fill in the entries for the Header file name, Init function, run function
	and set the queue size to at least 5 for I2CService.
	c) Add the new event types to the ES_EventType_t definiton. You can copy these 
	from the ES_Configure in the sample application. You need all of the events
	that begin with EV_I2C_	
	d) associate a timer with the I2C service by adding the PostI2CService function 
	to the definition of one of the timer response functions. 
	e) create a symbolic name (must be I2C_TIMER) and define it as the number of the
	timer that you chose at step 4
	f) add IsI2C0Finished to the EVENT_CHECK_LIST definition
4. Edit ES_EventCheckWrapper.h: add a #include for "I2CService.h"
5. Create a timer I2C_TEST_TIMER that posts to ColorSensor.c every 1 second (present way of polling).

 